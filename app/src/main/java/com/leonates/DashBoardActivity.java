package com.leonates;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.leonates.library.Constant;
import com.leonates.library.ItemClickSupport;
import com.leonates.library.Pref;
import com.squareup.picasso.Picasso;


public class DashBoardActivity extends AppCompatActivity {

    //First We Declare Titles And Icons For Our Navigation Drawer List View
    //This Icons And Titles Are holded in an Array as you can see


    //Similarly we Create a String Resource for the name and email in the header view
    //And we also create a int resource for profile picture in the header view

    // String NAME = "Akash Bangad";
    //  String EMAIL = "akash.bangad@android4devs.com";


    private Toolbar toolbar;                              // Declaring the Toolbar Object

    static TextView mTotalPoints, mLastPoints, mUserName;
    static ImageView mUserImage;

    static RecyclerView mRecyclerView;                           // Declaring RecyclerView
    RecyclerView.Adapter mAdapter;                        // Declaring Adapter For Recycler View
    RecyclerView.LayoutManager mLayoutManager;            // Declaring Layout Manager as a linear layout manager
    static DrawerLayout Drawer;                                  // Declaring DrawerLayout

    ActionBarDrawerToggle mDrawerToggle;                  // Declaring Action Bar Drawer Toggle


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_dashboard);

    /* Assinging the toolbar object to the view
    and setting the Action bar to our toolbar
     */
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {

            setSupportActionBar(toolbar);
        }
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        mTotalPoints = (TextView) findViewById(R.id.home_total);
        mLastPoints = (TextView) findViewById(R.id.home_last_pts);
        mUserImage = (ImageView) findViewById(R.id.home_user_photo);
        mUserName = (TextView) findViewById(R.id.home_user_name);

        mUserName.setText(Pref.getValue(DashBoardActivity.this, Constant.PREF_USER_FNAME, "") + " " + Pref.getValue(DashBoardActivity.this, Constant.PREF_USER_LNAME, ""));
        Picasso.with(DashBoardActivity.this).load(Pref.getValue(DashBoardActivity.this, Constant.PREF_USER_IMAGE, "")).placeholder(R.drawable.drawer_user).fit().centerInside().
                into(DashBoardActivity.mUserImage);

        Typeface typeFace = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Montserrat_Medium.otf");
        mTotalPoints.setTypeface(typeFace);
        mLastPoints.setTypeface(typeFace);
        mUserName.setTypeface(typeFace);

        mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView); // Assigning the RecyclerView Object to the xml View
        String TITLES[] = {getString(R.string.home), getString(R.string.contact), getString(R.string.furniture), getString(R.string.point), getString(R.string.profile), getString(R.string.terms), getString(R.string.tips), getString(R.string.tools), getString(R.string.logout)};
        int ICONS[] = {R.drawable.drawer_home, R.drawable.drawer_contact, R.drawable.drawer_furniture, R.drawable.drawer_points, R.drawable.drawer_profile, R.drawable.drawer_terms, R.drawable.drawer_tips, R.drawable.drawer_tools, R.drawable.logout};

        mAdapter = new DrawerAdapter(TITLES, ICONS, Pref.getValue(DashBoardActivity.this, Constant.PREF_USER_FNAME, "") + " " + Pref.getValue(DashBoardActivity.this, Constant.PREF_USER_LNAME, ""), Pref.getValue(DashBoardActivity.this, Constant.PREF_USER_EMAIL, ""), Pref.getValue(DashBoardActivity.this, Constant.PREF_USER_IMAGE, ""), DashBoardActivity.this, "user");       // Creating the Adapter of MyAdapter class(which we are going to see in a bit)
        // And passing the titles,icons,header view name, header view email,
        // and header view profile picture


        mRecyclerView.setAdapter(mAdapter);                              // Setting the adapter to RecyclerView
        mRecyclerView.setHasFixedSize(true);                            // Letting the system know that the list objects are of fixed size
        mLayoutManager = new LinearLayoutManager(this);                 // Creating a layout Manager

        mRecyclerView.setLayoutManager(mLayoutManager);                 // Setting the layout Manager


        Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout);        // Drawer object Assigned to the view

        mDrawerToggle = new ActionBarDrawerToggle(this, Drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

                // code here will execute once the drawer is opened( As I dont want anything happened whe drawer is
                // open I am not going to put anything here)
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                // Code here will execute once drawer is closed
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                try {

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                            INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(drawerView.getWindowToken(), 0);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }; // Drawer Toggle Object Made

        Drawer.setDrawerListener(mDrawerToggle); // Drawer Listener set to the Drawer toggle

        mDrawerToggle.syncState();               // Finally we set the drawer toggle sync State


        ItemClickSupport.addTo(mRecyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {

                displayView(position);

            }
        });


        displayView(1);


    }


    private void displayView(int position) {
        // update the main content by replacing fragments
        Fragment fragment = null;
        fragment = getSupportFragmentManager().findFragmentById(R.id.frame_container);
        switch (position) {
            case 1:
                if (fragment instanceof FragmentHome) {
                    Drawer.closeDrawer(mRecyclerView);
                } else {
                    fragment = new FragmentHome();

//                    mToolBarTitle.setText("Home");


                }
                break;
            case 2:
                Intent con = new Intent(DashBoardActivity.this, ActivityContact.class);
                startActivity(con);
                break;
            case 3:
                Intent i = new Intent(DashBoardActivity.this, ActivityFurniture.class);
                startActivity(i);
                break;
            case 4:
                Intent point = new Intent(DashBoardActivity.this, ActivityRedeemPoints.class);
                startActivity(point);
                break;
            case 5:
                Intent prof = new Intent(DashBoardActivity.this, ActivityEditProfile.class);
                startActivity(prof);
                break;
            case 6:
                Intent term = new Intent(DashBoardActivity.this, ActivityTerms.class);
                startActivity(term);
                break;
            case 7:
                Intent tips = new Intent(DashBoardActivity.this, ActivityTips.class);
                startActivity(tips);
                break;

            case 8:
                Intent tools = new Intent(DashBoardActivity.this, ActivityTools.class);
                startActivity(tools);
                break;
            case 9:
                new AlertDialog.Builder(DashBoardActivity.this)

                        .setMessage(getResources().getString(R.string.logout_app))
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                String devid = Pref.getValue(DashBoardActivity.this, Constant.PREF_DEVICE_ID, "");
                                Pref.deletAll(DashBoardActivity.this);
                                Pref.setValue(DashBoardActivity.this, Constant.PREF_DEVICE_ID, devid);
                                Intent logout = new Intent(DashBoardActivity.this, RegistrationActivity.class);
                                logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(logout);
                                finish();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing

                            }
                        })


                        .show();

                break;

            default:
                break;
        }

        if (fragment != null)

        {


            FragmentManager fragmentManager = getSupportFragmentManager();

            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, fragment).commit();

            // update selected item and title, then close the drawer


            Drawer.closeDrawer(mRecyclerView);

        } else {
            // error in creating fragment
            Log.e("ActivityFurniture", "Error in creating fragment");
        }

    }

    @Override
    protected void onResume() {
        mAdapter.notifyDataSetChanged();
        super.onResume();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                    INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(DashBoardActivity.this)

                .setMessage(getResources().getString(R.string.close_app))
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing

                    }
                })


                .show();
    }
}

