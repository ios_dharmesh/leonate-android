package com.leonates;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.leonates.library.ConnectionDetector;
import com.leonates.library.Constant;
import com.leonates.library.Pref;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.util.Locale;

import cz.msebera.android.httpclient.Header;
import io.fabric.sdk.android.Fabric;

/**
 * Created by sachin on 7/9/2017.
 */

public class ActivitySplash extends Activity {

    private BroadcastReceiver mRegistrationBroadcastReceiver;
    String deviceid = "";
    ConnectionDetector cd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_splash);
        Handler handler = new Handler();
        cd = new ConnectionDetector(ActivitySplash.this);

        final Runnable r = new Runnable() {
            public void run() {

                if (Pref.getValue(ActivitySplash.this, Constant.PREF_IS_LOGIN, "").equalsIgnoreCase("true")) {

                    if (cd.isConnectingToInternet()) {
                        getUserStatus();
                    } else {
                        if (Pref.getValue(ActivitySplash.this, Constant.PREF_LANG, "").equalsIgnoreCase("guj")) {
                            Configuration config = new Configuration();
                            config.locale = Locale.GERMANY;
                            getResources().updateConfiguration(config, getResources().getDisplayMetrics());
                            Intent i = new Intent(ActivitySplash.this, DashBoardActivity.class);
                            startActivity(i);
                            finish();

                        } else if (Pref.getValue(ActivitySplash.this, Constant.PREF_LANG, "").equalsIgnoreCase("hin")) {

                            Configuration config = new Configuration();
                            Locale locale = new Locale("hi");
                            config.locale = locale;
                            getResources().updateConfiguration(config, getResources().getDisplayMetrics());
                            Intent i = new Intent(ActivitySplash.this, DashBoardActivity.class);

                            startActivity(i);
                            finish();
                        } else {

                            Configuration config = new Configuration();
                            config.locale = Locale.ENGLISH;
                            getResources().updateConfiguration(config, getResources().getDisplayMetrics());
                            Intent i = new Intent(ActivitySplash.this, DashBoardActivity.class);
                            startActivity(i);
                            finish();
                        }

                    }


                } else {
                    new AsyncCallWS().execute();

                }


            }
        };

        handler.postDelayed(r, 2000);


    }

    private void getUserStatus() {

        AsyncHttpClient client = new AsyncHttpClient();
        try {
            client.setTimeout(40 * 1000);

            client.setMaxRetriesAndTimeout(2, 40 * 1000);
            RequestParams jsonParams = new RequestParams();
            jsonParams.put("apiKey", "w2e2rosrt3y5u6iterrest8iahug4h581e3");
            jsonParams.put("deviceType", "android");
            jsonParams.put("deviceToken", Pref.getValue(ActivitySplash.this, Constant.PREF_DEVICE_ID, ""));
            jsonParams.put("intUdId", "zsdffsdf");
            jsonParams.put("apiKey", "w2e2rosrt3y5u6iterrest8iahug4h581e3");
            jsonParams.put("userId", Pref.getValue(ActivitySplash.this, Constant.PREF_USER_ID, ""));


            client.post(ActivitySplash.this, Constant.server_path + "API/CheckUserStatus", jsonParams, new JsonHttpResponseHandler() {


                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    Log.e("getCategoryList", "getCategoryList" + response.toString());
                    String status = response.optString("status");
                    if (status.equalsIgnoreCase("True")) {
                        if (Pref.getValue(ActivitySplash.this, Constant.PREF_LANG, "").equalsIgnoreCase("guj")) {
                            Configuration config = new Configuration();
                            config.locale = Locale.GERMANY;
                            getResources().updateConfiguration(config, getResources().getDisplayMetrics());
                            Intent i = new Intent(ActivitySplash.this, DashBoardActivity.class);
                            startActivity(i);
                            finish();

                        } else if (Pref.getValue(ActivitySplash.this, Constant.PREF_LANG, "").equalsIgnoreCase("hin")) {

                            Configuration config = new Configuration();
                            Locale locale = new Locale("hi");
                            config.locale = locale;
                            getResources().updateConfiguration(config, getResources().getDisplayMetrics());
                            Intent i = new Intent(ActivitySplash.this, DashBoardActivity.class);

                            startActivity(i);
                            finish();
                        } else {

                            Configuration config = new Configuration();
                            config.locale = Locale.ENGLISH;
                            getResources().updateConfiguration(config, getResources().getDisplayMetrics());
                            Intent i = new Intent(ActivitySplash.this, DashBoardActivity.class);
                            startActivity(i);
                            finish();
                        }


                    } else if (status.equalsIgnoreCase("failed")) {
                        String message = response.optString("message");
                        Toast.makeText(ActivitySplash.this, message, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFinish() {

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);

                    //  Toast.makeText(ActivityScan.this, "Something went wrong!!!Please try again.", Toast.LENGTH_LONG).show();

                }


                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Log.e("onFailure,", "" + responseString);

                    //  Toast.makeText(ActivityScan.this, "Something went wrong!!!Please try again.", Toast.LENGTH_LONG).show();

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private class AsyncCallWS extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            String msg = "";
            try {

                deviceid = FirebaseInstanceId.getInstance().getToken();
                Log.e("refreshedToken", "refreshedToken " + msg);
                //    displayFirebaseRegId();

            } catch (Exception ex) {
                msg = "Error :";

            }
            return deviceid;
        }

        @Override
        protected void onPostExecute(String msg) {

            if (TextUtils.isEmpty(deviceid) && Pref.getValue(ActivitySplash.this, Constant.PREF_DEVICE_ID, "").equalsIgnoreCase("")) {
                new AsyncCallWS().execute();
            } else {
                Pref.setValue(ActivitySplash.this, Constant.PREF_DEVICE_ID, deviceid);
                Intent i = new Intent(ActivitySplash.this, RegistrationActivity.class);
                startActivity(i);
            }
        }
    }

    @Override
    protected void onPause() {

        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}
