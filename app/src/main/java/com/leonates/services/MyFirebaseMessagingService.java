package com.leonates.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.leonates.ActivityNotification;
import com.leonates.R;
import com.leonates.RegistrationActivity;
import com.leonates.library.Constant;
import com.leonates.library.Pref;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    public static final int MESSAGE_NOTIFICATION_ID = 435345;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        if (remoteMessage.getData() != null) {

            try {
                //JSONObject json = new JSONObject(remoteMessage.getData().get("msg"));
                createNotification(remoteMessage);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }

        }
    }


    private void createNotification(RemoteMessage json) {
        try {
            // JSONObject data = json.getJSONObject("data");

            //String title = json.getData().get("title");
            String message = json.getData().get("message");
            Context context = getBaseContext();
            Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
            mBuilder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher)).setContentTitle("Leonate Laminates")
                    .setSmallIcon(R.mipmap.noti)
                    .setColor(getResources().getColor(R.color.colorDrawerbg))
                    .setContentText(message)
                    .setPriority(2)
                    .setAutoCancel(true)
                    .setSound(soundUri)

                    .setPriority(Notification.PRIORITY_HIGH);


            Intent notificationIntent;
            NotificationManager mNotificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);

            if (Pref.getValue(this, Constant.PREF_IS_LOGIN, "").equalsIgnoreCase("true")) {
                notificationIntent = new Intent(context, ActivityNotification.class);
            } else {
                notificationIntent = new Intent(context, RegistrationActivity.class);
            }


            notificationIntent.putExtra("flag", "noti");

            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                    Intent.FLAG_ACTIVITY_CLEAR_TASK |
                    Intent.FLAG_ACTIVITY_CLEAR_TOP);

            PendingIntent resultPendingIntent =
                    PendingIntent.getActivity(
                            this,
                            0,
                            notificationIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );
            mBuilder.setContentIntent(resultPendingIntent);
            Notification notification = mBuilder.build();
            int smallIconId = context.getResources().getIdentifier("right_icon", "id", android.R.class.getPackage().getName());
            if (smallIconId != 0) {
                notification.contentView.setViewVisibility(smallIconId, View.GONE);

            }
            mNotificationManager.notify(MESSAGE_NOTIFICATION_ID, notification);


        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

}
