package com.leonates;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.leonates.library.ConnectionDetector;
import com.leonates.library.Constant;
import com.leonates.library.Pref;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * Created by sachin on 6/27/2017.
 */

public class ActivityOtp extends AppCompatActivity {

    EditText mOtpEdt;
    TextView mConfirmOtpBtn;
    ProgressDialog dialog;
    String code, number;
    ConnectionDetector cd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        if (getIntent().getExtras() != null) {

            code = intent.getStringExtra("code");
            number = intent.getStringExtra("number");
        }
        setContentView(R.layout.activity_otp);
        mOtpEdt = (EditText) findViewById(R.id.otp_edt);
        Typeface typeFace = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Montserrat_Medium.otf");
        mOtpEdt.setTypeface(typeFace);
        cd = new ConnectionDetector(ActivityOtp.this);
        mOtpEdt.setText(code);

        mConfirmOtpBtn = (TextView) findViewById(R.id.otp_btn);

        mConfirmOtpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(mOtpEdt.getText().toString())) {
                    Toast.makeText(ActivityOtp.this, "Please enter otp", Toast.LENGTH_SHORT).show();
                } else {
                    if (cd.isConnectingToInternet()) {
                        confirmOtp();
                    } else {
                        Toast.makeText(ActivityOtp.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });
    }

    private void confirmOtp() {
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();
        AsyncHttpClient client = new AsyncHttpClient();
        try {


            RequestParams jsonParams = new RequestParams();

            jsonParams.put("mobileNumber", number);

            jsonParams.put("verificationCode", mOtpEdt.getText().toString());
            jsonParams.put("deviceType", "android");
            jsonParams.put("deviceToken", Pref.getValue(ActivityOtp.this, Constant.PREF_DEVICE_ID, ""));
            jsonParams.put("intUdId", "zsdffsdf");
            jsonParams.put("apiKey", "w2e2rosrt3y5u6iterrest8iahug4h581e3");
            jsonParams.put("userId", Pref.getValue(ActivityOtp.this, Constant.PREF_USER_ID, ""));

            client.setTimeout(40 * 1000);

            client.setMaxRetriesAndTimeout(2, 40 * 1000);
            client.post(ActivityOtp.this, Constant.server_path + "API/CheckVerification", jsonParams, new JsonHttpResponseHandler() {


                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);

                    Log.e("xsvhkdsv", "sdkjnfhvcds" + response.toString());
                    String status = response.optString("status");
                    String errorcode = response.optString("errorcode");


                    if (status.equalsIgnoreCase("True")) {
                        try {
                            JSONArray mUserArray = response.optJSONArray("data");
                            for (int i = 0; i < mUserArray.length(); i++) {
                                JSONObject mUserObject = mUserArray.optJSONObject(i);

                                String user_id = mUserObject.optString("userId");

                                String fname = mUserObject.optString("firstName");
                                String lname = mUserObject.optString("lastName");
                                String email = mUserObject.optString("emailId");
                                String image = mUserObject.optString("profilePic");
                                String contact_number = mUserObject.optString("mobileNumber");
                                Pref.setValue(ActivityOtp.this, Constant.PREF_USER_ID, user_id);
                                Pref.setValue(ActivityOtp.this, Constant.PREF_USER_FNAME, fname);
                                Pref.setValue(ActivityOtp.this, Constant.PREF_USER_LNAME, lname);
                                Pref.setValue(ActivityOtp.this, Constant.PREF_USER_EMAIL, email);
                                Pref.setValue(ActivityOtp.this, Constant.PREF_USER_IMAGE, image);
                                Pref.setValue(ActivityOtp.this, Constant.PREF_USER_NO, contact_number);
                                Log.e("mobileNumber", "mobileNumber" + contact_number.toString());
                                Pref.setValue(ActivityOtp.this, Constant.PREF_IS_LOGIN, "true");

                            }
                            if (errorcode.equalsIgnoreCase("-1")) {
                                Pref.setValue(ActivityOtp.this, Constant.PREF_LANG, "eng");
                                Intent i = new Intent(ActivityOtp.this, ActivityUserStatus.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                                finish();
                            } else {
                                Pref.setValue(ActivityOtp.this, Constant.PREF_LANG, "eng");
                                Intent i = new Intent(ActivityOtp.this, DashBoardActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                                finish();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    } else if (status.equalsIgnoreCase("failed")) {
                        String message = response.optString("msg");
                        Toast.makeText(ActivityOtp.this, message, Toast.LENGTH_LONG).show();

                    }
                }

                @Override
                public void onFinish() {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    Toast.makeText(ActivityOtp.this, "Something went wrong!!!Please try again.", Toast.LENGTH_LONG).show();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Log.e("onFailure,", "" + responseString);
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


}
