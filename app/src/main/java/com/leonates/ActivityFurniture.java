package com.leonates;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.leonates.library.ConnectionDetector;
import com.leonates.library.Constant;
import com.leonates.library.DatabaseHandler;
import com.leonates.library.Pref;
import com.leonates.adapter.CategoryAdapter;
import com.leonates.model.CategoryModel;
import com.leonates.model.ProductModel;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class ActivityFurniture extends AppCompatActivity {
    ListView mList;
    ProgressDialog dialog;
    ArrayList<CategoryModel> mCategoryList = new ArrayList<CategoryModel>();
    static ArrayList<ProductModel> mProductList = new ArrayList<ProductModel>();
    CategoryAdapter mCategoryAdapter;
    DatabaseHandler db;
    Toolbar toolbar;
    ConnectionDetector cd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        toolbar = (Toolbar) findViewById(R.id.cat_toolbar);
        if (toolbar != null) {

            ImageView imgBack = (ImageView) toolbar.findViewById(R.id.cat_back_iv);
            TextView catTitle = (TextView) toolbar.findViewById(R.id.cat_title);
            Typeface typeFace = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Montserrat_Medium.otf");
            catTitle.setTypeface(typeFace);
            imgBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

            setSupportActionBar(toolbar);
        }
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        cd = new ConnectionDetector(ActivityFurniture.this);
        mList = (ListView) findViewById(R.id.lst);
        db = new DatabaseHandler(ActivityFurniture.this);
        if (db.getProductList() > 0) {
            db.deleteProductTable();
        }
        if (cd.isConnectingToInternet()) {
            getCategoryList();
        } else {
            Toast.makeText(ActivityFurniture.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }


        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(ActivityFurniture.this, ActivityProductList.class);
                i.putExtra("catid", mCategoryList.get(position).getCatId());
                i.putExtra("catname", mCategoryList.get(position).getCatName());
                startActivity(i);
            }
        });

    }

    private void getCategoryList() {
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();
        AsyncHttpClient client = new AsyncHttpClient();
        try {
            client.setTimeout(40 * 1000);

            client.setMaxRetriesAndTimeout(2, 40 * 1000);
            RequestParams jsonParams = new RequestParams();
            jsonParams.put("apiKey", "w2e2rosrt3y5u6iterrest8iahug4h581e3");
            jsonParams.put("deviceType", "android");
            jsonParams.put("deviceToken", Pref.getValue(ActivityFurniture.this, Constant.PREF_DEVICE_ID, ""));
            jsonParams.put("intUdId", "zsdffsdf");
            jsonParams.put("apiKey", "w2e2rosrt3y5u6iterrest8iahug4h581e3");
            jsonParams.put("userId", Pref.getValue(ActivityFurniture.this, Constant.PREF_USER_ID, ""));


            client.post(ActivityFurniture.this, Constant.server_path + "API/AllProducts", jsonParams, new JsonHttpResponseHandler() {


                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    Log.e("getCategoryList", "getCategoryList" + response.toString());
                    String status = response.optString("status");
                    String errorcode = response.optString("errorcode");

                    if (errorcode.equalsIgnoreCase("-1")) {

                        String devid = Pref.getValue(ActivityFurniture.this, Constant.PREF_DEVICE_ID, "");
                        Pref.deletAll(ActivityFurniture.this);
                        Pref.setValue(ActivityFurniture.this, Constant.PREF_DEVICE_ID, devid);
                        Intent logout = new Intent(ActivityFurniture.this, RegistrationActivity.class);
                        logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(logout);
                        finish();
                    } else {
                        if (status.equalsIgnoreCase("True")) {
                            if (mCategoryList.size() > 0) {
                                mCategoryList.clear();
                            }
                            if (mProductList.size() > 0) {
                                mProductList.clear();
                            }
                            final SQLiteDatabase mDb = db.getWritableDatabase();
                            mDb.beginTransaction();

                            JSONArray categoriesArray = response.optJSONArray("categories");
                            JSONArray productsArray = response.optJSONArray("products");
                            for (int i = 0; i < categoriesArray.length(); i++) {
                                JSONObject catsObject = categoriesArray.optJSONObject(i);
                                String catId = catsObject.optString("catId");
                                String catName = catsObject.optString("catName");
                                String catImage = catsObject.optString("catImage");

                                mCategoryList.add(new CategoryModel(catId, catName, catImage));
                            }
                            try {
                                for (int i = 0; i < productsArray.length(); i++) {
                                    JSONObject productsObject = productsArray.optJSONObject(i);
                                    String categoryproductId = productsObject.optString("categoryproductId");
                                    String categoryProductName = productsObject.optString("categoryProductName");
                                    String image1 = productsObject.optString("image1");

                                    String catId = productsObject.optString("catId");
                                    String catName = productsObject.optString("catName");
                                    String catImage = productsObject.optString("catImage");

                                    ContentValues contentValues = new ContentValues();
                                    contentValues.put("categoryproductid", categoryproductId);
                                    contentValues.put("categoryproductname", categoryProductName);
                                    contentValues.put("image", image1);
                                    contentValues.put("catid", catId);
                                    contentValues.put("catname", catName);
                                    contentValues.put("catimage", catImage);

                                    mDb.insert("productlist", null, contentValues);

                                    //   mProductList.add(new ProductModel(categoryproductId, categoryProductName, image1, catId,catName, catImage));

                                }
                                mDb.setTransactionSuccessful();
                            } catch (Exception e) {
                                e.printStackTrace();
                            } finally {
                                //End the transaction
                                mDb.endTransaction();

                                mCategoryAdapter = new CategoryAdapter(ActivityFurniture.this, mCategoryList);
                                mList.setAdapter(mCategoryAdapter);
                            }

                        } else if (status.equalsIgnoreCase("failed")) {
                            String message = response.optString("message");
                            Toast.makeText(ActivityFurniture.this, message, Toast.LENGTH_LONG).show();
                        }
                    }
                }

                @Override
                public void onFinish() {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    Toast.makeText(ActivityFurniture.this, "Something went wrong!!!Please try again.", Toast.LENGTH_LONG).show();

                }


                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Log.e("onFailure,", "" + responseString);
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    Toast.makeText(ActivityFurniture.this, "Something went wrong!!!Please try again.", Toast.LENGTH_LONG).show();

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
