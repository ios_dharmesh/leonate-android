package com.leonates;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.leonates.library.ConnectionDetector;
import com.leonates.library.Constant;
import com.leonates.library.Pref;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;


public class FragmentHome extends Fragment {

    //Ui Elements
    RelativeLayout mFurnitureRl, mScanRl, mTipsRl, mHistoryRL, mNotificationRl, mToolsRl, mRedeemRl;
    TextView mFurnitureTV, mScanTV, mTipsTV, mHistoryTV, mNotificationTV, mToolsTV, mRedeemTV;
    ProgressDialog dialog;
    ConnectionDetector cd;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, null);
        mFurnitureRl = (RelativeLayout) rootView.findViewById(R.id.home_furniture_rl);
        mScanRl = (RelativeLayout) rootView.findViewById(R.id.home_scan_rl);
        mTipsRl = (RelativeLayout) rootView.findViewById(R.id.home_tips_rl);
        mHistoryRL = (RelativeLayout) rootView.findViewById(R.id.home_his_rl);
        mNotificationRl = (RelativeLayout) rootView.findViewById(R.id.home_noti_rl);
        mToolsRl = (RelativeLayout) rootView.findViewById(R.id.home_tools_rl);
        mRedeemRl = (RelativeLayout) rootView.findViewById(R.id.home_redeem_rl);

        mFurnitureTV = (TextView) rootView.findViewById(R.id.home_furniture_tv);
        mScanTV = (TextView) rootView.findViewById(R.id.home_scan_tv);
        mTipsTV = (TextView) rootView.findViewById(R.id.home_tips_tv);
        mHistoryTV = (TextView) rootView.findViewById(R.id.home_history_tv);
        mNotificationTV = (TextView) rootView.findViewById(R.id.home_noti_tv);
        mToolsTV = (TextView) rootView.findViewById(R.id.home_tools_tv);
        mRedeemTV= (TextView) rootView.findViewById(R.id.home_redeem_tv);



        Typeface typeFace = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Montserrat_Medium.otf");
        mFurnitureTV.setTypeface(typeFace);
        mScanTV.setTypeface(typeFace);
        mTipsTV.setTypeface(typeFace);
        mHistoryTV.setTypeface(typeFace);
        mNotificationTV.setTypeface(typeFace);
        mToolsTV.setTypeface(typeFace);
        mRedeemTV.setTypeface(typeFace);



        cd = new ConnectionDetector(getActivity());

        mHistoryRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), ActivityHistory.class);
                startActivity(i);
            }
        });
        mTipsRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), ActivityTips.class);
                startActivity(i);
            }
        });

        mNotificationRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), ActivityNotification.class);
                i.putExtra("flag", "");
                startActivity(i);
            }
        });
        mFurnitureRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), ActivityFurniture.class);
                startActivity(i);
            }
        });

        mToolsRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), ActivityTools.class);
                startActivity(i);
            }
        });
        mScanRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), ActivityScan.class);
                startActivity(i);
            }
        });
        mRedeemRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), ActivityRedeemPoints.class);
                startActivity(i);
            }
        });



        return rootView;
    }

    @Override
    public void onResume() {
        if (cd.isConnectingToInternet()) {
            getUserPoints();
        } else {
            DashBoardActivity.mTotalPoints.setText(Pref.getValue(getActivity(), Constant.PREF_TOTAL_POINTS, "0"));
            DashBoardActivity.mLastPoints.setText(Pref.getValue(getActivity(), Constant.PREF_LAST_POINTS, "0"));


        }

       /* DashBoardActivity.mTotalPoints.setText(Pref.getValue(getActivity(), Constant.PREF_TOTAL_POINTS, "0"));
        DashBoardActivity.mLastPoints.setText(Pref.getValue(getActivity(), Constant.PREF_LAST_POINTS, "0"));
*/
        super.onResume();
    }

    private void getUserPoints() {
        dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();
        AsyncHttpClient client = new AsyncHttpClient();
        try {
            client.setTimeout(40 * 1000);

            client.setMaxRetriesAndTimeout(2, 40 * 1000);
            RequestParams jsonParams = new RequestParams();
            jsonParams.put("apiKey", "w2e2rosrt3y5u6iterrest8iahug4h581e3");
            jsonParams.put("deviceType", "android");
            jsonParams.put("deviceToken", Pref.getValue(getActivity(), Constant.PREF_DEVICE_ID, ""));
            jsonParams.put("intUdId", "zsdffsdf");
            jsonParams.put("apiKey", "w2e2rosrt3y5u6iterrest8iahug4h581e3");
            jsonParams.put("userId", Pref.getValue(getActivity(), Constant.PREF_USER_ID, ""));


            client.post(getActivity(), Constant.server_path + "API/UserPoints", jsonParams, new JsonHttpResponseHandler() {


                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    Log.e("getCategoryList", "getCategoryList" + response.toString());
                    String status = response.optString("status");
                    String errorcode = response.optString("errorcode");

                    if (errorcode.equalsIgnoreCase("-1")) {

                        String devid = Pref.getValue(getActivity(), Constant.PREF_DEVICE_ID, "");
                        Pref.deletAll(getActivity());
                        Pref.setValue(getActivity(), Constant.PREF_DEVICE_ID, devid);
                        Intent logout = new Intent(getActivity(), RegistrationActivity.class);
                        logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(logout);
                        getActivity().finish();
                    } else {
                        if (status.equalsIgnoreCase("True")) {


                            try {
                                String totalPoints = response.optString("totalPoints");
                                String lastMonth = response.optString("lastMonth");
                                Pref.setValue(getActivity(), Constant.PREF_TOTAL_POINTS, totalPoints);

                                Pref.setValue(getActivity(), Constant.PREF_LAST_POINTS, lastMonth);

                                DashBoardActivity.mTotalPoints.setText(Pref.getValue(getActivity(), Constant.PREF_TOTAL_POINTS, "0"));
                                DashBoardActivity.mLastPoints.setText(Pref.getValue(getActivity(), Constant.PREF_LAST_POINTS, "0"));


                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else if (status.equalsIgnoreCase("false")) {
                            String message = response.optString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                        }
                    }
                }

                @Override
                public void onFinish() {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    Toast.makeText(getActivity(), "Something went wrong!!!Please try again.", Toast.LENGTH_LONG).show();

                }


                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Log.e("onFailure,", "" + responseString);
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    Toast.makeText(getActivity(), "Something went wrong!!!Please try again.", Toast.LENGTH_LONG).show();

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

}
