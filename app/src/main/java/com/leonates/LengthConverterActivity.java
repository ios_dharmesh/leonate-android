package com.leonates;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.leonates.library.convert;

import cz.msebera.android.httpclient.util.TextUtils;

/**
 * Created by Pelicans on 6/20/2017.
 */

public class LengthConverterActivity extends AppCompatActivity {
    private Spinner fromSpinner;
    private Spinner toSpinner;
    private EditText inputValue;
     TextView convertResult,mFromHintTv,mToHintTv,mInputHint,mResultHint;
    private convert lengthUnit;
    Toolbar toolbar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_length_converter);
        toolbar = (Toolbar) findViewById(R.id.length_toolbar);
        if (toolbar != null) {

            ImageView imgBack = (ImageView) toolbar.findViewById(R.id.length_back_iv);
            TextView title = (TextView) toolbar.findViewById(R.id.lenght_title);
            Typeface typeFace = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Montserrat_Medium.otf");
            title.setTypeface(typeFace);
            imgBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

            setSupportActionBar(toolbar);
        }
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        fromSpinner = (Spinner)findViewById(R.id.length_from_spinner);
        toSpinner = (Spinner)findViewById(R.id.length_to_spinner);
        inputValue = (EditText)findViewById(R.id.length_unit_value);
        convertResult = (TextView)findViewById(R.id.length_convert_result);

        mFromHintTv = (TextView)findViewById(R.id.length_first_binary);
        mToHintTv = (TextView)findViewById(R.id.length_to_unit);
        mInputHint = (TextView)findViewById(R.id.length_enter_a_number);
        mResultHint = (TextView)findViewById(R.id.length_result_hint);

        Typeface typeFace = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Montserrat_Medium.otf");
        inputValue.setTypeface(typeFace);
        mFromHintTv.setTypeface(typeFace);
        mToHintTv.setTypeface(typeFace);
        mInputHint.setTypeface(typeFace);
        mResultHint.setTypeface(typeFace);

        ArrayAdapter<CharSequence> staticAdapter = ArrayAdapter.createFromResource(this, R.array.length_units, android.R.layout.simple_spinner_item);
        staticAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fromSpinner.setAdapter(staticAdapter);
        toSpinner.setAdapter(staticAdapter);
        toSpinner.setSelection(1);
     final Button convertButton = (Button)findViewById(R.id.length_calculation_button);
        convertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                            INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(convertButton.getWindowToken(), 0);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                checkIfFieldIsEmpty(inputValue);
                String convertFromUnit = fromSpinner.getSelectedItem().toString();
                String convertToUnit = toSpinner.getSelectedItem().toString();
                double userInputValue = returnErrorInputs(inputValue);
                lengthUnit = new convert();
                double returnedConvertedResult = lengthUnit.convert(convertFromUnit, convertToUnit, userInputValue);
                convertResult.setText(String.valueOf(String.format("%.2f", returnedConvertedResult))+" "+toSpinner.getSelectedItem().toString());
            }
        });

    }
    private double returnErrorInputs(EditText input){
        double returnedInput = 0.0;
        String inputValue = input.getText().toString();
        if(inputValue.equals(".")){
            returnedInput = 0.0;
        }
        else if(inputValue.contains("..")){
            returnedInput = 0.0;
        }else if(TextUtils.isEmpty(inputValue)){
            returnedInput = 0.0;
        }
        else{
            returnedInput = Double.parseDouble(inputValue);
        }
        return returnedInput;
    }
    private void checkIfFieldIsEmpty(EditText checkContent){
        if(checkContent.getText().toString().equals("")){
            Toast.makeText(LengthConverterActivity.this, "All the input field must be filled", Toast.LENGTH_LONG).show();
            return;
        }
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                    INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
