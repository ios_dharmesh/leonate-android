package com.leonates;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

import static com.loopj.android.http.AsyncHttpClient.log;

public class ActivityWeather extends AppCompatActivity implements LocationListener {

    ProgressDialog dialog;
    String lat, lang;
    String provider;
    Context context = this;
    TextView humidity, temperature, mWeatherHint;
    LocationManager locationManager;
    public static final String COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    public static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    public static final int PERMISSION_REQUEST_CODE = 200;
    private static final String URL = "http://api.openweathermap.org/data/2.5/weather?";
    private static final String KEY = "a7723f9c8360abb8b4550dac5f6d94c1";
    Toolbar toolbar;
    // flag for GPS status
    boolean isGPSEnabled = false;
    private static final int LOCATION_REQUEST_CODE = 21;
    // flag for network status
    boolean isNetworkEnabled = false;
    boolean isCall = false,isApi=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        toolbar = (Toolbar) findViewById(R.id.weather_toolbar);
        if (toolbar != null) {

            ImageView imgBack = (ImageView) toolbar.findViewById(R.id.weather_back_iv);
            TextView title = (TextView) toolbar.findViewById(R.id.weather_title);
            Typeface typeFace = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Montserrat_Medium.otf");
            title.setTypeface(typeFace);
            imgBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

            setSupportActionBar(toolbar);
        }
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        humidity = (TextView) findViewById(R.id.humidity);
        temperature = (TextView) findViewById(R.id.temp);
        mWeatherHint = (TextView) findViewById(R.id.weather_hint);

        Typeface typeFace = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Montserrat_Medium.otf");
        humidity.setTypeface(typeFace);
        temperature.setTypeface(typeFace);
        mWeatherHint.setTypeface(typeFace);


        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);

        requestpermission();

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                requestpermission();
               /* if (lang != null && lat != null) {
                    isCall = true;
                    getWeather();
                } else {
                    Toast.makeText(this, "lat long not found", Toast.LENGTH_SHORT).show();
                }*/
            } else {
                requestpermission();
            }
        } else if (requestCode == LOCATION_REQUEST_CODE) {

            requestpermission();
        /*    locationManager = (LocationManager) getApplicationContext()
                    .getSystemService(LOCATION_SERVICE);
            // getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);


            if (provider != null && !provider.equals("")) {

                Location location = locationManager.getLastKnownLocation(provider);

                locationManager.requestLocationUpdates(provider, 20000, 1, this);

                if (location != null) {
                    onLocationChanged(location);
                    if (lang != null && lat != null) {
                        isCall=true;
                        getWeather();
                    } else {
                        Toast.makeText(this, "lat long not found", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    //  Toast.makeText(getBaseContext(), "Location can't be retrieved", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getBaseContext(), "No Provider Found", Toast.LENGTH_SHORT).show();
            }*/

        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE: {


                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    requestpermission();
                } else {


                    Toast.makeText(ActivityWeather.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }


        }


        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    private void requestpermission() {

        boolean permission = (ContextCompat.checkSelfPermission(ActivityWeather.this,
                COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(ActivityWeather.this,
                FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);
        if (permission) {
            locationManager = (LocationManager) getApplicationContext()
                    .getSystemService(LOCATION_SERVICE);
            // getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);


            if (isGPSEnabled && isNetworkEnabled) {

                if (provider != null && !provider.equals("")) {

                    Location location = locationManager.getLastKnownLocation(provider);

                    locationManager.requestLocationUpdates(provider, 1000000000, 1000, this);

                    if (location != null) {
                        if (!isCall) {
                            onLocationChanged(location);
                        }
                        if (lang != null && lat != null && !isApi) {
                            log.e("fgxdgsdgsdzfgvsdfg", "sfsdfsfs"+isApi);
                            isApi=true;
                            getWeather();


                        } else {
                           // Toast.makeText(this, "Please wait we are fetching your location.", Toast.LENGTH_SHORT).show();
                        }

                    } else {

                        //     Toast.makeText(getBaseContext(), "Location can't be retrieved", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getBaseContext(), "No Provider Found", Toast.LENGTH_SHORT).show();
                }
            } else {
                showSettingsAlert();
            }


        } else {
            ActivityCompat.requestPermissions(ActivityWeather.this,
                    new String[]{COARSE_LOCATION, FINE_LOCATION},
                    PERMISSION_REQUEST_CODE);
        }


        // ActivityCompat.requestPermissions((Activity) context, new String[]{COARSE_LOCATION, FINE_LOCATION}, PERMISSION_REQUEST_CODE);
    }


    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ActivityWeather.this);

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, LOCATION_REQUEST_CODE);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                finish();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }


    @Override
    public void onLocationChanged(Location location) {

        lat = String.valueOf(location.getLatitude());
        lang = String.valueOf(location.getLongitude());
        Log.e("LAT", String.valueOf(lat));
        Log.e("Long", String.valueOf(lang));
        locationManager.removeUpdates(this);
        if (isCall) {
        } else {
            isCall = true;
            requestpermission();
        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


    private void getWeather() {
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();
        AsyncHttpClient client = new AsyncHttpClient();
        try {


            client.setTimeout(40 * 1000);
            Log.e("RESPONSE", "dfsfsdf" + "APPID=" + KEY + "&lat=" + lat + "&lon=" + lang + "&units=metric");
            client.setMaxRetriesAndTimeout(2, 40 * 1000);
            client.get(ActivityWeather.this, URL + "APPID=" + KEY + "&lat=" + lat + "&lon=" + lang + "&units=metric", new JsonHttpResponseHandler() {


                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }

                    isApi = true;

                    JSONObject temp = response.optJSONObject("main");


                    temperature.setText("Temperature :- " + temp.optString("temp"));
                    humidity.setText("Humidity :- " + temp.optString("humidity"));

                }


                @Override
                public void onFinish() {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    Toast.makeText(ActivityWeather.this, "Something went wrong!!!Please try again.", Toast.LENGTH_LONG).show();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Log.e("onFailure,", "" + responseString);
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}


