package com.leonates;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.leonates.library.ConnectionDetector;
import com.leonates.library.Constant;
import com.leonates.library.Pref;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * Created by sachin on 7/16/2017.
 */

public class ActivityRedeemDetail extends AppCompatActivity {
    TextView mGiftDetailTv, mRedeemPointsTv,mRedeemTitleTv;
    ImageView mGiftIv;
    String mId, mImage, mDesc,mName;
    ProgressDialog dialog;
    ConnectionDetector cd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        if (getIntent().getExtras() != null) {


            mId = intent.getStringExtra("id");
            mImage = intent.getStringExtra("image");
            mDesc = intent.getStringExtra("desc");
            mName = intent.getStringExtra("name");

        }


        setContentView(R.layout.activity_gift_details);

        Toolbar toolbar = (Toolbar) findViewById(R.id.redeem_detail_toolbar);
        if (toolbar != null) {

            ImageView imgBack = (ImageView) toolbar.findViewById(R.id.redeem_detail_back_iv);
            TextView title = (TextView) toolbar.findViewById(R.id.redeem_detail_title);
            Typeface typeFace = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Montserrat_Medium.otf");
            title.setTypeface(typeFace);
            imgBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

            setSupportActionBar(toolbar);

        }
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        mGiftDetailTv = (TextView) findViewById(R.id.redeem_detail_tv);
        mGiftDetailTv.setMovementMethod(new ScrollingMovementMethod());
        mRedeemPointsTv = (TextView) findViewById(R.id.redeem_detail_redeem_pts);
        mRedeemTitleTv = (TextView) findViewById(R.id.redeem_name_tv);

        Typeface typeFace = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Montserrat_Medium.otf");
        mGiftDetailTv.setTypeface(typeFace);
        mRedeemPointsTv.setTypeface(typeFace);
        mRedeemTitleTv.setTypeface(typeFace);

        mGiftIv = (ImageView) findViewById(R.id.redeem_detail_iv);
        cd = new ConnectionDetector(ActivityRedeemDetail.this);
        mRedeemTitleTv.setText(mName);
        mGiftDetailTv.setText(mDesc);
        Picasso.with(ActivityRedeemDetail.this)
                .load(mImage)
                .placeholder(R.drawable.placeholder)
                .resize(500, 200)
                .centerInside()

                .into(mGiftIv);
        mRedeemPointsTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                            INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(mRedeemPointsTv.getWindowToken(), 0);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (cd.isConnectingToInternet()) {
                    redeemPointst();
                } else {
                    Toast.makeText(ActivityRedeemDetail.this, "No internet connection.", Toast.LENGTH_LONG).show();

                }
            }
        });
    }

    private void redeemPointst() {
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();
        AsyncHttpClient client = new AsyncHttpClient();
        try {


            RequestParams jsonParams = new RequestParams();

            jsonParams.put("userId", Pref.getValue(ActivityRedeemDetail.this, Constant.PREF_USER_ID, ""));
            jsonParams.put("giftId", mId);
            jsonParams.put("deviceType", "android");
            jsonParams.put("deviceToken", Pref.getValue(ActivityRedeemDetail.this, Constant.PREF_DEVICE_ID, ""));
            jsonParams.put("intUdId", "zsdffsdf");
            jsonParams.put("apiKey", "w2e2rosrt3y5u6iterrest8iahug4h581e3");

            Log.e("jsonParams", "jsonParams" + jsonParams.toString());
            client.setTimeout(40 * 1000);

            client.setMaxRetriesAndTimeout(2, 40 * 1000);
            client.post(ActivityRedeemDetail.this, Constant.server_path + "API/RedeemPoints", jsonParams, new JsonHttpResponseHandler() {


                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    Log.e("redeemPointst", "redeemPointst" + response.toString());
                    String status = response.optString("status");
                    String errorcode = response.optString("errorcode");

                    if (errorcode.equalsIgnoreCase("-1")) {

                        String devid = Pref.getValue(ActivityRedeemDetail.this, Constant.PREF_DEVICE_ID, "");
                        Pref.deletAll(ActivityRedeemDetail.this);
                        Pref.setValue(ActivityRedeemDetail.this, Constant.PREF_DEVICE_ID, devid);
                        Intent logout = new Intent(ActivityRedeemDetail.this, RegistrationActivity.class);
                        logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(logout);
                        finish();
                    } else {
                        if (status.equalsIgnoreCase("True")) {
                            try {
                                Toast.makeText(ActivityRedeemDetail.this, "Your points redeemed successfully", Toast.LENGTH_LONG).show();

                                String totalPoints = response.optString("totalPoints");
                                String lastMonth = response.optString("lastMonth");
                                Pref.setValue(ActivityRedeemDetail.this, Constant.PREF_TOTAL_POINTS, totalPoints);

                                Pref.setValue(ActivityRedeemDetail.this, Constant.PREF_LAST_POINTS, lastMonth);


                                finish();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            if (dialog != null && dialog.isShowing()) {
                                dialog.dismiss();
                            }

                        } else if (status.equalsIgnoreCase("failed")) {
                            String message = response.optString("msg");
                            Toast.makeText(ActivityRedeemDetail.this, message, Toast.LENGTH_LONG).show();
                        }
                    }
                }

                @Override
                public void onFinish() {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    Toast.makeText(ActivityRedeemDetail.this, "Something went wrong!!!Please try again.", Toast.LENGTH_LONG).show();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Log.e("onFailure,", "" + responseString);
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                    INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

}
