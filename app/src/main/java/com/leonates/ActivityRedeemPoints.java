package com.leonates;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.leonates.adapter.RedeemPointsAdapter;
import com.leonates.library.ConnectionDetector;
import com.leonates.library.Constant;
import com.leonates.library.DatabaseHandler;
import com.leonates.library.Pref;
import com.leonates.model.GiftModel;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

/**
 * Created by sachin on 7/7/2017.
 */

public class ActivityRedeemPoints extends AppCompatActivity {
    ProgressDialog dialog;
    RedeemPointsAdapter mRedeemPointsAdapter;
    ArrayList<GiftModel> mRedeemList = new ArrayList<GiftModel>();
    ArrayList<GiftModel> mFilterLst = new ArrayList<GiftModel>();
    ConnectionDetector cd;
    DatabaseHandler db;
    ListView mList;
    EditText mSearchEdtTxt;
    ImageView mCancleSearchImg, mFilterIv;
    TextView mTotalPoints, mLastPoints, mUserName;
    ImageView mUserImage;
    Toolbar toolbar;
    ListView dialog_ListView;
    String[] listContent = {
            "All", "0 to 299", "300 to 599",
            "600 to 899", "900 to 1199", "1200 to 1499", "1500 to 1799",
            "1800 to 2099", "2100 to 4099", "4100 to 6099", "6100 to 8099", "8100 to 10099",
            "10100 to 12099", "12100 to 14099", "14100 to 16099", "16100 to 18099", "18100 to 20099",
            "20100 to 22099", "22100 to 30099", "30100 to 40099",
            "40100 to 50099", "50100 to 60099", "60100 to 96099", "96100 to 100099"};
    Dialog mDialog = null;
    boolean isCross = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_redeempoints);

        toolbar = (Toolbar) findViewById(R.id.redeem_toolbar);
        if (toolbar != null) {

            ImageView imgBack = (ImageView) toolbar.findViewById(R.id.redeem_back_iv);
            TextView title = (TextView) toolbar.findViewById(R.id.redeem_title);
            Typeface typeFace = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Montserrat_Medium.otf");
            title.setTypeface(typeFace);
            imgBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
            mFilterIv = (ImageView) toolbar.findViewById(R.id.redeem_filter_iv);
            mFilterIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDialog();
                }
            });
            setSupportActionBar(toolbar);
        }
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mList = (ListView) findViewById(R.id.redeem_lst);
        mTotalPoints = (TextView) findViewById(R.id.redeem_total);
        mLastPoints = (TextView) findViewById(R.id.redeem_last_pts);
        TextView lastHintTv = (TextView) findViewById(R.id.redeem_last_hint);
        TextView totalHintTv = (TextView) findViewById(R.id.redeem_total_hint);
        mUserImage = (ImageView) findViewById(R.id.redeem_user_photo);
        mSearchEdtTxt = (EditText) findViewById(R.id.search_edit_txt);
        mCancleSearchImg = (ImageView) findViewById(R.id.search_cncl_img);
        mUserName = (TextView) findViewById(R.id.redeem_user_name);


        Typeface typeFace = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Montserrat_Medium.otf");
        mTotalPoints.setTypeface(typeFace);
        mLastPoints.setTypeface(typeFace);
        lastHintTv.setTypeface(typeFace);
        totalHintTv.setTypeface(typeFace);
        mSearchEdtTxt.setTypeface(typeFace);
        mUserName.setTypeface(typeFace);

        mUserName.setText(Pref.getValue(ActivityRedeemPoints.this, Constant.PREF_USER_FNAME, "") + " " + Pref.getValue(ActivityRedeemPoints.this, Constant.PREF_USER_LNAME, ""));

        mTotalPoints.setText(Pref.getValue(ActivityRedeemPoints.this, Constant.PREF_TOTAL_POINTS, "0"));
        mLastPoints.setText(Pref.getValue(ActivityRedeemPoints.this, Constant.PREF_LAST_POINTS, "0"));
        if (TextUtils.isEmpty(Pref.getValue(ActivityRedeemPoints.this, Constant.PREF_USER_IMAGE, ""))) {
        } else {
            Picasso.with(ActivityRedeemPoints.this).load(Pref.getValue(ActivityRedeemPoints.this, Constant.PREF_USER_IMAGE, "")).placeholder(R.drawable.drawer_user).fit().centerInside().
                    into(mUserImage);
        }

        cd = new ConnectionDetector(ActivityRedeemPoints.this);
        db = new DatabaseHandler(ActivityRedeemPoints.this);
        isCross = false;
        if (cd.isConnectingToInternet()) {
            db.deleteGiftsTable();

            getRedeemList();
        } else {
            Toast.makeText(ActivityRedeemPoints.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent i = new Intent(ActivityRedeemPoints.this, ActivityRedeemDetail.class);
                i.putExtra("image", mRedeemList.get(position).getGiftImage());
                i.putExtra("desc", mRedeemList.get(position).getGiftDesc());
                i.putExtra("id", mRedeemList.get(position).getGiftsId());
                i.putExtra("name", mRedeemList.get(position).getGiftName());
                startActivity(i);
            }
        });

        mSearchEdtTxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                if (mSearchEdtTxt.getText().length() > 0) {
                    mCancleSearchImg.setImageDrawable(getResources().getDrawable(R.drawable.close));
                    String text = mSearchEdtTxt.getText().toString().toLowerCase(Locale.getDefault());
                    if (mRedeemList.size() > 0) {
                        isCross = true;
                        mRedeemPointsAdapter.filter(text);
                    } else {

                        isCross = true;
                        if (mRedeemList.size() > 0) {
                            mRedeemList.clear();
                        }
                        mRedeemList = db.getGiftsList();
                        mRedeemPointsAdapter = new RedeemPointsAdapter(ActivityRedeemPoints.this, mRedeemList);
                        mList.setAdapter(mRedeemPointsAdapter);
                        mRedeemPointsAdapter.notifyDataSetChanged();
                        mRedeemPointsAdapter.filter(text);

                    }
                } else {
                    isCross = false;
                    mCancleSearchImg.setImageDrawable(getResources().getDrawable(R.drawable.search));
                    if (mRedeemList.size() > 0) {
                        mRedeemList.clear();
                    }
                    mRedeemList = db.getGiftsList();
                    mRedeemPointsAdapter = new RedeemPointsAdapter(ActivityRedeemPoints.this, mRedeemList);
                    mList.setAdapter(mRedeemPointsAdapter);
                    mRedeemPointsAdapter.notifyDataSetChanged();
                }

            }
        });
        mCancleSearchImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isCross) {
                    mSearchEdtTxt.getText().clear();
                    if (mRedeemList.size() > 0) {
                        mRedeemList.clear();
                    }
                    mRedeemList = db.getGiftsList();
                    mRedeemPointsAdapter = new RedeemPointsAdapter(ActivityRedeemPoints.this, mRedeemList);
                    mList.setAdapter(mRedeemPointsAdapter);
                    mRedeemPointsAdapter.notifyDataSetChanged();
                }
            }
        });

    }


    @Override
    protected void onResume() {

        mTotalPoints.setText(Pref.getValue(ActivityRedeemPoints.this, Constant.PREF_TOTAL_POINTS, "0"));
        mLastPoints.setText(Pref.getValue(ActivityRedeemPoints.this, Constant.PREF_LAST_POINTS, "0"));

        super.onResume();
    }

    private void getRedeemList() {
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();
        AsyncHttpClient client = new AsyncHttpClient();
        try {


            RequestParams jsonParams = new RequestParams();

            jsonParams.put("userId", Pref.getValue(ActivityRedeemPoints.this, Constant.PREF_USER_ID, ""));

            jsonParams.put("deviceType", "android");
            jsonParams.put("deviceToken", Pref.getValue(ActivityRedeemPoints.this, Constant.PREF_DEVICE_ID, ""));
            jsonParams.put("intUdId", "zsdffsdf");
            jsonParams.put("apiKey", "w2e2rosrt3y5u6iterrest8iahug4h581e3");

            Log.e("jsonParams", "jsonParams" + jsonParams.toString());
            client.setTimeout(40 * 1000);

            client.setMaxRetriesAndTimeout(2, 40 * 1000);
            client.post(ActivityRedeemPoints.this, Constant.server_path + "API/GiftList", jsonParams, new JsonHttpResponseHandler() {


                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    Log.e("xsvhkdsv", "sdkjnfhvcds" + response.toString());
                    String status = response.optString("status");
                    String errorcode = response.optString("errorcode");

                    if (errorcode.equalsIgnoreCase("-1")) {

                        String devid = Pref.getValue(ActivityRedeemPoints.this, Constant.PREF_DEVICE_ID, "");
                        Pref.deletAll(ActivityRedeemPoints.this);
                        Pref.setValue(ActivityRedeemPoints.this, Constant.PREF_DEVICE_ID, devid);
                        Intent logout = new Intent(ActivityRedeemPoints.this, RegistrationActivity.class);
                        logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(logout);
                        finish();
                    } else {
                        if (status.equalsIgnoreCase("True")) {
                            if (mRedeemList.size() > 0) {
                                mRedeemList.clear();
                            }
                            final SQLiteDatabase mDb = db.getWritableDatabase();
                            mDb.beginTransaction();
                            try {


                                JSONArray NotiArray = response.optJSONArray("data");
                                for (int i = 0; i < NotiArray.length(); i++) {
                                    JSONObject notiObject = NotiArray.optJSONObject(i);
                                    String giftsId = notiObject.optString("giftsId");
                                    String points = notiObject.optString("points");
                                    String giftImage = notiObject.optString("giftImage");
                                    String giftName = notiObject.optString("giftName");
                                    String giftDesc = notiObject.optString("giftDesc");

                                    ContentValues contentValues = new ContentValues();
                                    contentValues.put("giftsId", giftsId);
                                    contentValues.put("points", points);
                                    contentValues.put("giftImage", giftImage);
                                    contentValues.put("giftName", giftName);
                                    contentValues.put("giftDesc", giftDesc);

                                    mDb.insert("gifts", null, contentValues);

                                }


                                mDb.setTransactionSuccessful();
                            } catch (Exception e) {
                                e.printStackTrace();
                            } finally {
                                //End the transaction
                                mDb.endTransaction();


                                if (dialog != null && dialog.isShowing()) {
                                    dialog.dismiss();
                                }

                                mRedeemList = db.getGiftsList();

                                mFilterLst.clear();
                                mFilterLst.addAll(mRedeemList);
                                mRedeemPointsAdapter = new RedeemPointsAdapter(ActivityRedeemPoints.this, mRedeemList);
                                mList.setAdapter(mRedeemPointsAdapter);
                            }
                        } else if (status.equalsIgnoreCase("failed")) {
                            String message = response.optString("msg");
                            Toast.makeText(ActivityRedeemPoints.this, message, Toast.LENGTH_LONG).show();
                        }
                    }
                }

                @Override
                public void onFinish() {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    Toast.makeText(ActivityRedeemPoints.this, "Something went wrong!!!Please try again.", Toast.LENGTH_LONG).show();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Log.e("onFailure,", "" + responseString);
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void showDialog() {
        mDialog = new Dialog(ActivityRedeemPoints.this);
        mDialog.setContentView(R.layout.dialog_layout);
        mDialog.setTitle("Custom Dialog");

        mDialog.setCancelable(true);
        mDialog.setCanceledOnTouchOutside(true);

        mDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialog) {
                // TODO Auto-generated method stub

            }
        });

        mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {

            @Override
            public void onDismiss(DialogInterface dialog) {
                // TODO Auto-generated method stub

            }
        });

        //Prepare ListView in dialog
        dialog_ListView = (ListView) mDialog.findViewById(R.id.dialoglist);
        ArrayAdapter<String> adapter
                = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, listContent);
        dialog_ListView.setAdapter(adapter);
        dialog_ListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                mDialog.dismiss();
                if (mRedeemList.size() > 0) {
                    mRedeemList.clear();
                }
                mRedeemList = db.getGiftsList();
                showfilterdata(position);
            }
        });
        mDialog.show();

    }

    public void filterPrice(String constraint, String constraint1) {
        mFilterLst.clear();
        mFilterLst.addAll(mRedeemList);
        mRedeemList.clear();
        if (constraint.length() == 0 || constraint1.length() == 0) {
            mRedeemList.addAll(mFilterLst);
        } else {
            for (GiftModel wp : mFilterLst) {
                if (Integer.valueOf(wp.getPoints()) > Integer.valueOf(constraint) && Integer.valueOf(wp.getPoints()) < Integer.valueOf(constraint1)) {
                    mRedeemList.add(wp);
                }
            }
        }
        mRedeemPointsAdapter = new RedeemPointsAdapter(ActivityRedeemPoints.this, mRedeemList);
        mList.setAdapter(mRedeemPointsAdapter);
        mRedeemPointsAdapter.notifyDataSetChanged();
    }

    public void showfilterdata(int id) {
        switch (id) {
            case 0:


                mRedeemPointsAdapter = new RedeemPointsAdapter(ActivityRedeemPoints.this, mRedeemList);
                mList.setAdapter(mRedeemPointsAdapter);
                mRedeemPointsAdapter.notifyDataSetChanged();
                break;
            case 1:

                filterPrice("0", "299");
                break;
            case 2:
                filterPrice("300", "599");
                break;
            case 3:
                filterPrice("600", "899");
                break;
            case 4:
                filterPrice("900", "1199");
                break;
            case 5:
                filterPrice("1200", "1499");

                break;
            case 6:
                filterPrice("1500", "1799");


                break;
            case 7:
                filterPrice("1800", "2099");

                break;
            case 8:
                filterPrice("2100", "4099");

                break;
            case 9:
                filterPrice("4100", "6099");
                break;
            case 10:
                filterPrice("6100", "8099");
                break;
            case 11:
                filterPrice("8100", "10099");
                break;
            case 12:
                filterPrice("10100", "12099");
                break;
            case 13:
                filterPrice("12100", "14099");
                break;
            case 14:

                filterPrice("14100", "16099");
                break;
            case 15:
                filterPrice("16100", "18099");
                break;
            case 16:
                filterPrice("18100", "20099");
                break;
            case 17:
                filterPrice("20100", "22099");
                break;
            case 18:
                filterPrice("22100", "30099");
                break;
            case 19:
                filterPrice("30100", "40099");
                break;
            case 20:
                filterPrice("40100", "50099");
                break;
            case 21:
                filterPrice("50100", "60099");
                break;
            case 22:
                filterPrice("60100", "96099");
                break;
            case 23:
                filterPrice("96100", "100099");
                break;
            default:
                break;
        }

    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                    INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
