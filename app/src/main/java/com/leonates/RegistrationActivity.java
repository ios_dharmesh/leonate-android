package com.leonates;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.leonates.library.ConnectionDetector;
import com.leonates.library.Constant;
import com.leonates.library.Pref;
import com.leonates.library.Utils;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Pelicans on 6/26/2017.
 */

public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener {
    //Ui Elemnts
    EditText mUserNumber, mUsername, mUserArea, mUserPwd, mCmfrmPwd;
    ImageView mUserProf;

    TextView mRegBtn;


    //Class Objects
    ProgressDialog dialog;
    ConnectionDetector cd;
    private static final int REQUEST_WRITE_STORAGE = 311;
    private static final int REQUEST_CAMERA = 312;
    private static final int REQUEST_GALLERY_STORAGE = 313;


    private static final String[] PERMS_TAKE_PICTURE = {
            android.Manifest.permission.CAMERA,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    String realPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        //Set Up Toolbar
        cd = new ConnectionDetector(RegistrationActivity.this);


        regLoadUi();


    }

    public void regLoadUi() {

        mUserNumber = (EditText) findViewById(R.id.reg_contact_no_edt);
        mRegBtn = (TextView) findViewById(R.id.reg_btn);
        mUsername = (EditText) findViewById(R.id.reg_full_name_edt);
        mUserArea = (EditText) findViewById(R.id.reg_area_edt);
        mUserPwd = (EditText) findViewById(R.id.reg_password_edt);
        mCmfrmPwd = (EditText) findViewById(R.id.reg_confirm_pwd_edt);
        mUserProf = (ImageView) findViewById(R.id.reg_prof_iv);

        Typeface typeFace = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Montserrat_Medium.otf");
        mUserNumber.setTypeface(typeFace);
        mUsername.setTypeface(typeFace);
        mUserArea.setTypeface(typeFace);
        mCmfrmPwd.setTypeface(typeFace);
        mUserPwd.setTypeface(typeFace);
        mRegBtn.setTypeface(typeFace);

        mRegBtn.setOnClickListener(this);
        mUserProf.setOnClickListener(this);

    }


    private void regUser() {
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();
        AsyncHttpClient client = new AsyncHttpClient();
        try {


            RequestParams jsonParams = new RequestParams();


            jsonParams.put("mobileNumber", mUserNumber.getText().toString());

            jsonParams.put("deviceType", "android");
            jsonParams.put("deviceToken", Pref.getValue(RegistrationActivity.this, Constant.PREF_DEVICE_ID, ""));
            jsonParams.put("intUdId", "zsdffsdf");
            jsonParams.put("apiKey", "w2e2rosrt3y5u6iterrest8iahug4h581e3");

            Log.e("jsonParams", "jsonParams" + jsonParams.toString());
            client.setTimeout(40 * 1000);

            client.setMaxRetriesAndTimeout(2, 40 * 1000);
            client.post(RegistrationActivity.this, Constant.server_path + "API/Register", jsonParams, new JsonHttpResponseHandler() {


                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);

                    Log.e("xsvhkdsv", "sdkjnfhvcds" + response.toString());
                    String status = response.optString("status");
                    if (status.equalsIgnoreCase("True")) {
                        String contact_number = "", code = "";
                        try {
                            JSONArray mUserArray = response.optJSONArray("data");
                            for (int i = 0; i < mUserArray.length(); i++) {
                                JSONObject mUserObject = mUserArray.optJSONObject(i);

                                String user_id = mUserObject.optString("userId");


                                contact_number = mUserObject.optString("mobileNumber");
                                code = mUserObject.optString("verificationCode");
                                Pref.setValue(RegistrationActivity.this, Constant.PREF_USER_ID, user_id);


                            }
                            Intent i = new Intent(RegistrationActivity.this, ActivityOtp.class);
                            i.putExtra("code", code);
                            i.putExtra("number", contact_number);
                            startActivity(i);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    } else if (status.equalsIgnoreCase("failed")) {
                        String message = response.optString("msg");
                        Toast.makeText(RegistrationActivity.this, message, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFinish() {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    Toast.makeText(RegistrationActivity.this, "Something went wrong!!!Please try again.", Toast.LENGTH_LONG).show();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Log.e("onFailure,", "" + responseString);
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @Override
    public void onBackPressed() {
        finish();

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.reg_btn:
                if (cd.isConnectingToInternet()) {
                    userValidation();
                } else {
                    Toast.makeText(RegistrationActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }


                break;
            case R.id.reg_prof_iv:
                checkPermission();


                break;

        }
    }

    public void checkPermission() {
        boolean hasPermission = (ContextCompat.checkSelfPermission(RegistrationActivity.this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(RegistrationActivity.this,
                android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED);

        if (!hasPermission) {
            ActivityCompat.requestPermissions(RegistrationActivity.this, PERMS_TAKE_PICTURE,
                    REQUEST_WRITE_STORAGE);
        } else {
            selectImage();
        }
    }
    private void selectImage() {

        final CharSequence[] items = {"Take Photo", "Choose from Library", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(RegistrationActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface pdialog, int item) {
                if (items[item].equals("Take Photo")) {

                    final String dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/Pearltz/";
                    File newdir = new File(dir);
                    if (newdir.exists()) {
                    } else {
                        newdir.mkdir();
                    }

                    String fileName = System.currentTimeMillis() + ".jpg";

                    File filee = new File(dir, fileName);

                    if (Build.VERSION.SDK_INT > 22) {
                        Uri photoURI = FileProvider.getUriForFile(RegistrationActivity.this,
                                BuildConfig.APPLICATION_ID + ".provider",
                                filee);
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                        Pref.setValue(RegistrationActivity.this, Constant.PREF_TEMP_IMG, filee.getAbsolutePath());
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);

                        startActivityForResult(intent, REQUEST_CAMERA);
                    } else {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                        Pref.setValue(RegistrationActivity.this, Constant.PREF_TEMP_IMG, filee.getAbsolutePath());
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(filee.getAbsoluteFile()));

                        startActivityForResult(intent, REQUEST_CAMERA);
                    }
                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            REQUEST_GALLERY_STORAGE);
                } else if (items[item].equals("Cancel")) {
                    pdialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void userValidation() {
        //  if (!TextUtils.isEmpty(realPath)) {
        if (!TextUtils.isEmpty(mUserNumber.getText().toString().trim()) && mUserNumber.getText().toString().length() > 9) {


            regUser();

        } else {
            Toast.makeText(RegistrationActivity.this, "Please Enter valid mobile number (max 10 digits).", Toast.LENGTH_LONG).show();
        }


    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                    INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode == RESULT_OK) {

            if (requestCode == REQUEST_CAMERA) {
                realPath = Utils.rotateProdImage(Pref.getValue(RegistrationActivity.this, Constant.PREF_TEMP_IMG, ""));
                Log.e("CAMERA PATH", realPath);


                Picasso.with(RegistrationActivity.this).load(new File(Utils.rotateProdImage(realPath))).resize(120, 120).centerInside().
                        into(mUserProf);


            } else if (requestCode == REQUEST_GALLERY_STORAGE) {
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                assert selectedImage != null;
                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                assert cursor != null;
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                realPath = cursor.getString(columnIndex);
                Log.e("gallery PATH", realPath);
                cursor.close();


                Picasso.with(RegistrationActivity.this).load(new File(realPath)).resize(120, 120).centerInside().
                        into(mUserProf);


            }
        }
    }

}
