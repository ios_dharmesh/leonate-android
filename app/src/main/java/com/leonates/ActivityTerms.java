package com.leonates;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.leonates.library.ConnectionDetector;
import com.leonates.library.Constant;

/**
 * Created by Pelicans on 7/11/2017.
 */

public class ActivityTerms extends AppCompatActivity {

    WebView mTermWv;
    Toolbar toolbar;
    ConnectionDetector cd;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);
        toolbar = (Toolbar) findViewById(R.id.term_toolbar);
        if (toolbar != null) {

            ImageView imgBack = (ImageView) toolbar.findViewById(R.id.term_back_iv);
            TextView title = (TextView) toolbar.findViewById(R.id.term_title);
            Typeface typeFace = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Montserrat_Medium.otf");
            title.setTypeface(typeFace);
            imgBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

            setSupportActionBar(toolbar);
        }
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        cd = new ConnectionDetector(ActivityTerms.this);
        mTermWv=(WebView)findViewById(R.id.term_webview);
        mTermWv.setWebViewClient(new MyBrowser());
        mTermWv.getSettings().setLoadsImagesAutomatically(true);

        mTermWv.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        mTermWv.loadUrl(Constant.server_path+"uploads/Terms_and_condition.html");
    }


    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            return super.shouldOverrideUrlLoading(view, request);
        }
    }
}
