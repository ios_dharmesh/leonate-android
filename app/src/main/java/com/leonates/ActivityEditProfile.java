package com.leonates;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.leonates.library.ConnectionDetector;
import com.leonates.library.Constant;
import com.leonates.library.Pref;
import com.leonates.library.Utils;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Pelicans on 6/26/2017.
 */

public class ActivityEditProfile extends AppCompatActivity implements View.OnClickListener {
    //Ui Elemnts
    EditText mUSerEmailEdt, mUserNumber, mUserFname, mUserLname;
    RadioButton mEngRb, mGujRb, mHindiRb;
    RadioGroup langRg;
    private Toolbar toolbar;
    CircleImageView mUserImg;
    TextView mRegBtn;

    //Variables
    String realPath = "";
    private static final int REQUEST_WRITE_STORAGE = 211;
    private static final int REQUEST_CAMERA = 212;
    private static final int REQUEST_GALLERY_STORAGE = 213;
    Uri fileUri;

    //Class Objects
    ProgressDialog dialog;
    private static final String[] PERMS_TAKE_PICTURE = {
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    ConnectionDetector cd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        //Set Up Toolbar
        toolbar = (Toolbar) findViewById(R.id.edt_prof_toolbar);
        if (toolbar != null) {

            ImageView imgBack = (ImageView) toolbar.findViewById(R.id.edt_prof_back_iv);
            TextView title = (TextView) toolbar.findViewById(R.id.edt_prof_title);
            Typeface typeFace = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Montserrat_Medium.otf");
            title.setTypeface(typeFace);

            imgBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

            setSupportActionBar(toolbar);
        }
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        regLoadUi();


    }

    public void regLoadUi() {
        mUserFname = (EditText) findViewById(R.id.edtprof_user_fname_edt);
        mUserLname = (EditText) findViewById(R.id.edtprof_user_lname_edt);
        mUSerEmailEdt = (EditText) findViewById(R.id.edtprof_user_email_edt);

        mUserNumber = (EditText) findViewById(R.id.edtprof_contact_no_edt);
        mRegBtn = (TextView) findViewById(R.id.edtprof_btn);
        mUserImg = (CircleImageView) findViewById(R.id.edtprof_user_photo);

        mEngRb = (RadioButton) findViewById(R.id.edtprof_eng_rb);
        mGujRb = (RadioButton) findViewById(R.id.edtprof_guj_rb);
        mHindiRb = (RadioButton) findViewById(R.id.edtprof_hindi_rb);
        TextView mPhotoHintTv = (TextView) findViewById(R.id.edtprof_photo_hint_tv);
        TextView mLangHintTv = (TextView) findViewById(R.id.edtprof_lang_hint_tv);


        Typeface typeFace = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Montserrat_Medium.otf");
        mUserFname.setTypeface(typeFace);
        mUserLname.setTypeface(typeFace);
        mUSerEmailEdt.setTypeface(typeFace);
        mUserNumber.setTypeface(typeFace);
        mRegBtn.setTypeface(typeFace);
        mEngRb.setTypeface(typeFace);
        mGujRb.setTypeface(typeFace);
        mHindiRb.setTypeface(typeFace);
        mPhotoHintTv.setTypeface(typeFace);
        mLangHintTv.setTypeface(typeFace);

        cd = new ConnectionDetector(ActivityEditProfile.this);
        if (Pref.getValue(ActivityEditProfile.this, Constant.PREF_LANG, "").equalsIgnoreCase("guj")) {
            mGujRb.setChecked(true);
        } else if (Pref.getValue(ActivityEditProfile.this, Constant.PREF_LANG, "").equalsIgnoreCase("hin")) {
            mHindiRb.setChecked(true);
        } else {
            mEngRb.setChecked(true);
        }
        langRg = (RadioGroup) findViewById(R.id.radioGroup_lang);


        mUserFname.setText(Pref.getValue(ActivityEditProfile.this, Constant.PREF_USER_FNAME, ""));
        mUserFname.setSelection(mUserFname.getText().length());
        mUserLname.setText(Pref.getValue(ActivityEditProfile.this, Constant.PREF_USER_LNAME, ""));
        mUSerEmailEdt.setText(Pref.getValue(ActivityEditProfile.this, Constant.PREF_USER_EMAIL, ""));
        mUserNumber.setText(Pref.getValue(ActivityEditProfile.this, Constant.PREF_USER_NO, ""));
        if (TextUtils.isEmpty(Pref.getValue(ActivityEditProfile.this, Constant.PREF_USER_IMAGE, ""))) {
        } else {
            Picasso.with(ActivityEditProfile.this).load(Pref.getValue(ActivityEditProfile.this, Constant.PREF_USER_IMAGE, "")).placeholder(R.drawable.drawer_user).fit().centerInside().
                    into(mUserImg);
        }
        mRegBtn.setOnClickListener(this);
        mUserImg.setOnClickListener(this);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_WRITE_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // selectImage();
                    checkPermission();

                } else {
                    Toast.makeText(ActivityEditProfile.this, "The app was not allowed to write to your storage. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show();
                }
            }
        }

    }

    public void checkPermission() {
        boolean hasPermission = (ContextCompat.checkSelfPermission(ActivityEditProfile.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(ActivityEditProfile.this,
                Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED);

        if (!hasPermission) {
            ActivityCompat.requestPermissions(ActivityEditProfile.this, PERMS_TAKE_PICTURE,
                    REQUEST_WRITE_STORAGE);
        } else {

            selectImage();
        }
    }

    private void selectImage() {

        final CharSequence[] items = {"Take Photo", "Choose from Library", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityEditProfile.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {

                    try {
                        final String dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/Leonates";
                        File newdir = new File(dir);
                        if (newdir.exists()) {
                        } else {
                            newdir.mkdir();
                        }

                        String fileName = System.currentTimeMillis() + ".jpg";
                        //      String path = Environment.getExternalStorageDirectory().toString();
                        File filee = new File(dir, fileName);

                       /* Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE, FileProvider.getUriForFile(ActivityEditProfile.this,
                                "com.example.android.fileprovider",
                                filee));*/
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        Pref.setValue(ActivityEditProfile.this, Constant.PREF_TEMP_IMG, filee.getAbsolutePath());

                        intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(
                                ActivityEditProfile.this,
                                "com.example.android.fileprovider",
                                filee));


                        startActivityForResult(intent, REQUEST_CAMERA);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            REQUEST_GALLERY_STORAGE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode == RESULT_OK) {

            if (requestCode == REQUEST_CAMERA) {
                // realPath = Pref.getValue(RegistrationActivity.this, Constant.PREF_TEMP_IMG, "");
                realPath = Pref.getValue(ActivityEditProfile.this, Constant.PREF_TEMP_IMG, "");

                Picasso.with(ActivityEditProfile.this).load(new File(Utils.rotateProdImage(realPath))).resize(400, 400).centerInside().
                        into(mUserImg);
                Pref.setValue(ActivityEditProfile.this, Constant.PREF_TEMP_IMG, "");

            } else if (requestCode == REQUEST_GALLERY_STORAGE) {
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = null;
                cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                if (cursor != null) {
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    realPath = cursor.getString(columnIndex);
                    cursor.close();
                    Picasso.with(ActivityEditProfile.this).load(new File(realPath)).fit().centerInside().
                            into(mUserImg);
                    //mUserImg.setImageBitmap(BitmapFactory.decodeFile(realPath));
                }
            }
        }

    }

    private void regUser() {
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();
        AsyncHttpClient client = new AsyncHttpClient();
        try {


            RequestParams jsonParams = new RequestParams();

            jsonParams.put("firstName", mUserFname.getText().toString());
            jsonParams.put("lastName", mUserLname.getText().toString());

            jsonParams.put("emailId", mUSerEmailEdt.getText().toString());
            jsonParams.put("mobileNumber", mUserNumber.getText().toString());
            jsonParams.put("userId", Pref.getValue(ActivityEditProfile.this, Constant.PREF_USER_ID, ""));


            jsonParams.put("deviceType", "android");
            jsonParams.put("deviceToken", Pref.getValue(ActivityEditProfile.this, Constant.PREF_DEVICE_ID, ""));
            jsonParams.put("intUdId", "zsdffsdf");
            jsonParams.put("apiKey", "w2e2rosrt3y5u6iterrest8iahug4h581e3");
            if (realPath.isEmpty()) {


            } else {
                jsonParams.put("profilePic", new File(realPath));
            }
            Log.e("jsonParams", "jsonParams" + jsonParams.toString());
            client.setTimeout(40 * 1000);


            client.setMaxRetriesAndTimeout(2, 40 * 1000);
            client.post(ActivityEditProfile.this, Constant.server_path + "API/EditProfile", jsonParams, new JsonHttpResponseHandler() {


                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    Log.e("xsvhkdsv", "sdkjnfhvcds" + response.toString());
                    String status = response.optString("status");
                    String errorcode = response.optString("errorcode");

                    if (errorcode.equalsIgnoreCase("-1")) {

                        String devid = Pref.getValue(ActivityEditProfile.this, Constant.PREF_DEVICE_ID, "");
                        Pref.deletAll(ActivityEditProfile.this);
                        Pref.setValue(ActivityEditProfile.this, Constant.PREF_DEVICE_ID, devid);
                        Intent logout = new Intent(ActivityEditProfile.this, RegistrationActivity.class);
                        logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(logout);
                        finish();
                    } else {
                        if (status.equalsIgnoreCase("True")) {
                            try {
                                JSONArray mUserArray = response.optJSONArray("data");
                                for (int i = 0; i < mUserArray.length(); i++) {
                                    JSONObject mUserObject = mUserArray.optJSONObject(i);

                                    String user_id = mUserObject.optString("userId");

                                    String fname = mUserObject.optString("firstName");
                                    String lname = mUserObject.optString("lastName");
                                    String email = mUserObject.optString("emailId");
                                    String image = mUserObject.optString("profilePic");

                                    String contact_number = mUserObject.optString("mobileNumber");
                                    Pref.setValue(ActivityEditProfile.this, Constant.PREF_USER_ID, user_id);

                                    Pref.setValue(ActivityEditProfile.this, Constant.PREF_USER_IMAGE, image);


                                    Pref.setValue(ActivityEditProfile.this, Constant.PREF_USER_FNAME, fname);
                                    Pref.setValue(ActivityEditProfile.this, Constant.PREF_USER_LNAME, lname);
                                    Pref.setValue(ActivityEditProfile.this, Constant.PREF_USER_EMAIL, email);
                                    Pref.setValue(ActivityEditProfile.this, Constant.PREF_USER_NO, contact_number);

                                    Log.e("mobileNumber", "mobileNumber" + contact_number.toString());

                                }

                                if (mEngRb.isChecked() && Pref.getValue(ActivityEditProfile.this, Constant.PREF_LANG, "").equalsIgnoreCase("eng")) {
                                    finish();
                                } else if (mGujRb.isChecked() && Pref.getValue(ActivityEditProfile.this, Constant.PREF_LANG, "").equalsIgnoreCase("guj")) {
                                    finish();
                                } else if (mHindiRb.isChecked() && Pref.getValue(ActivityEditProfile.this, Constant.PREF_LANG, "").equalsIgnoreCase("hin")) {
                                    finish();
                                } else {
                                    if (mEngRb.isChecked()) {
                                        Pref.setValue(ActivityEditProfile.this, Constant.PREF_LANG, "eng");
                                        Configuration config = new Configuration();
                                        config.locale = Locale.ENGLISH;
                                        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
                                        Intent i = new Intent(ActivityEditProfile.this, DashBoardActivity.class);
                                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(i);
                                    } else if (mHindiRb.isChecked()) {
                                        Pref.setValue(ActivityEditProfile.this, Constant.PREF_LANG, "hin");
                                        Configuration config = new Configuration();
                                        Locale locale = new Locale("hi");

                                        config.locale = locale;
                                        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
                                        Intent i = new Intent(ActivityEditProfile.this, DashBoardActivity.class);
                                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(i);
                                    } else {
                                        Pref.setValue(ActivityEditProfile.this, Constant.PREF_LANG, "guj");
                                        Configuration config = new Configuration();
                                        config.locale = Locale.GERMANY;
                                        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
                                        Intent i = new Intent(ActivityEditProfile.this, DashBoardActivity.class);
                                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(i);
                                    }
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (status.equalsIgnoreCase("failed")) {
                            String message = response.optString("msg");
                            Toast.makeText(ActivityEditProfile.this, message, Toast.LENGTH_LONG).show();
                        }

                    }
                }

                @Override
                public void onFinish() {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    Toast.makeText(ActivityEditProfile.this, "Something went wrong!!!Please try again.", Toast.LENGTH_LONG).show();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Log.e("onFailure,", "" + responseString);
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @Override
    public void onBackPressed() {
        finish();

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edtprof_btn:
                try {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                            INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(mRegBtn.getWindowToken(), 0);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                userValidation();
                break;
            case R.id.edtprof_user_photo:
                checkPermission();

                break;
        }
    }


    public void userValidation() {
        //  if (!TextUtils.isEmpty(realPath)) {
        if (!TextUtils.isEmpty(mUserFname.getText().toString().trim())) {
            if (!TextUtils.isEmpty(mUserLname.getText().toString().trim())) {
                if (!TextUtils.isEmpty(mUSerEmailEdt.getText().toString().trim())) {
                    if (Utils.isValidAlphabetEmail(mUSerEmailEdt.getText().toString().trim())) {
                        if (!TextUtils.isEmpty(mUserNumber.getText().toString().trim()) && mUserNumber.getText().toString().length() > 9) {

                            if (cd.isConnectingToInternet()) {
                                regUser();
                            } else {
                                Toast.makeText(ActivityEditProfile.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                            }


                        } else {
                            Toast.makeText(ActivityEditProfile.this, "Please Enter valid mobile number (max 10 digits).", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(ActivityEditProfile.this, "Please Enter Valid Email Address", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(ActivityEditProfile.this, "Please Enter Your Email Address", Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(ActivityEditProfile.this, "Please Enter Last Name", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(ActivityEditProfile.this, "Please Enter First Name", Toast.LENGTH_LONG).show();
        }
    /*} else {
        Toast.makeText(RegistrationActivity.this, "Please Add Profile Image", Toast.LENGTH_LONG).show();
    }*/


    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                    INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


}
