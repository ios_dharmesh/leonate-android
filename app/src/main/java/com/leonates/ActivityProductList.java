package com.leonates;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.leonates.library.DatabaseHandler;
import com.leonates.model.ProductAdapter;
import com.leonates.model.ProductModel;

import java.util.ArrayList;

/**
 * Created by Pelicans on 6/20/2017.
 */

public class ActivityProductList extends AppCompatActivity {

    GridView mProdListGV;
    ProductAdapter mProductAdapter;
    String catidid = "", catName = "";
    static ArrayList<ProductModel> mProductList = new ArrayList<ProductModel>();
    DatabaseHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        if (getIntent().getExtras() != null) {

            catidid = intent.getStringExtra("catid");
            catName = intent.getStringExtra("catname");
        }


        setContentView(R.layout.activity_product_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.prod_list_toolbar);
        if (toolbar != null) {
            TextView mTiotle = (TextView) toolbar.findViewById(R.id.prod_list_title);
            mTiotle.setText(catName);
            Typeface typeFace = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Montserrat_Medium.otf");
            mTiotle.setTypeface(typeFace);
            ImageView imgBack = (ImageView) toolbar.findViewById(R.id.prod_list_back_iv);
            imgBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

            setSupportActionBar(toolbar);

        }
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        mProdListGV = (GridView) findViewById(R.id.prod_list_gv);

        db = new DatabaseHandler(ActivityProductList.this);
        if (mProductList.size() > 0) {
            mProductList.clear();
        }
        mProductList = db.getProductList(catidid);
        if (mProductList.size() > 0) {
            mProductAdapter = new ProductAdapter(ActivityProductList.this, mProductList);
            mProdListGV.setAdapter(mProductAdapter);
        }

        mProdListGV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent i1 = new Intent(ActivityProductList.this, SliderImageActivity.class);
                i1.putExtra("catName", catName);
                i1.putExtra("pos", i);
                startActivity(i1);
            }
        });


    }
}
