package com.leonates.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.leonates.R;
import com.leonates.model.GiftModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;


/**
 * Created by Pelicans on 3/18/2016.
 */
public class RedeemPointsAdapter extends BaseAdapter  {


    private Context mContext;
    ArrayList<GiftModel> coverList;
    ArrayList<GiftModel> arraylist;

    public RedeemPointsAdapter(Context c, ArrayList<GiftModel> coverList) {
        mContext = c;
        this.coverList = coverList;
        this.arraylist = new ArrayList<GiftModel>();
        this.arraylist.addAll(coverList);

    }

    @Override
    public int getCount() {
        return coverList.size();
    }

    @Override
    public GiftModel getItem(int position) {
        return coverList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        View grid = null;
        convertView = null;

        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            grid = new View(mContext);

            grid = inflater.inflate(R.layout.item_gifts_row, parent, false);

            TextView mPointsTxt = (TextView) grid.findViewById(R.id.gift_item_pts_tv);
            TextView mNameTxt = (TextView) grid.findViewById(R.id.gift_item_name_tv);

            ImageView mGiftIv = (ImageView) grid.findViewById(R.id.gift_item_iv);
            Typeface typeFace = Typeface.createFromAsset(mContext.getAssets(), "fonts/Montserrat_Medium.otf");
            mPointsTxt.setTypeface(typeFace);
            mNameTxt.setTypeface(typeFace);

            mPointsTxt.setText(coverList.get(position).getPoints() + "\n" + "Points");
            mNameTxt.setText(coverList.get(position).getGiftName());

            Picasso.with(mContext)
                    .load(coverList.get(position).getGiftImage())
                    .centerCrop()
                    .resize(200, 200)
                    .into(mGiftIv);


        }

        return grid;
    }
    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        coverList.clear();
        if (charText.length() == 0) {
            coverList.addAll(arraylist);
        } else {
            for (GiftModel wp : arraylist) {
                if (wp.getGiftName().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    coverList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
// Filter Class

    }




