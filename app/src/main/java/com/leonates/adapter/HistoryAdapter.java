package com.leonates.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.leonates.R;
import com.leonates.model.HistoryModel;
import com.leonates.model.NotificationModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


/**
 * Created by Pelicans on 3/18/2016.
 */
public class HistoryAdapter extends BaseAdapter {


    private Context mContext;
    ArrayList<HistoryModel> coverList;


    public HistoryAdapter(Context c, ArrayList<HistoryModel> coverList) {
        mContext = c;
        this.coverList = coverList;


    }

    @Override
    public int getCount() {
        return coverList.size();
    }

    @Override
    public HistoryModel getItem(int position) {
        return coverList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        View grid = null;
        convertView = null;

        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            grid = new View(mContext);

            grid = inflater.inflate(R.layout.item_his_row, parent, false);

            TextView mNotiTv = (TextView) grid.findViewById(R.id.item_his_tv);
            ImageView mGiftIv = (ImageView) grid.findViewById(R.id.item_his_iv);
            Typeface typeFace=Typeface.createFromAsset(mContext.getAssets(),"fonts/Montserrat_Medium.otf");
            mNotiTv.setTypeface(typeFace);
            mNotiTv.setText(coverList.get(position).getGiftName());
            Picasso.with(mContext)
                    .load(coverList.get(position).getGiftImage())
                    .centerCrop()
                    .resize(200, 200)
                    .into(mGiftIv);

        }

        return grid;
    }

}
