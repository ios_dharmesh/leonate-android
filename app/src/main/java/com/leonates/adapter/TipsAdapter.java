package com.leonates.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.leonates.R;
import com.leonates.model.TipsModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


/**
 * Created by Pelicans on 3/18/2016.
 */
public class TipsAdapter extends BaseAdapter {


    private Context mContext;

    private ArrayList<TipsModel> coverList1;


    public TipsAdapter(Context c, ArrayList<TipsModel> coverList) {
        mContext = c;
        this.coverList1 = coverList;


    }

    @Override
    public int getCount() {
        return coverList1.size();
    }

    @Override
    public TipsModel getItem(int position) {
        return coverList1.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        View grid = null;
        convertView = null;

        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            grid = new View(mContext);

            grid = inflater.inflate(R.layout.item_tips_row, parent, false);
            ImageView mProdIv = (ImageView) grid.findViewById(R.id.item_tips_list_img);
            Picasso.with(mContext)
                    .load("http://img.youtube.com/vi/" + coverList1.get(position).getVideoId() + "/0.jpg")
                    .placeholder(R.drawable.placeholder)
                    .fit()

                    .into(mProdIv);


        }

        return grid;
    }

}
