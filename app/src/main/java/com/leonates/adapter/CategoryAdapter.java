package com.leonates.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.leonates.R;
import com.leonates.model.CategoryModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


/**
 * Created by Pelicans on 3/18/2016.
 */
public class CategoryAdapter extends BaseAdapter {


    private Context mContext;
    ArrayList<CategoryModel> coverList;


    public CategoryAdapter(Context c, ArrayList<CategoryModel> coverList) {
        mContext = c;
        this.coverList = coverList;


    }

    @Override
    public int getCount() {
        return coverList.size();
    }

    @Override
    public CategoryModel getItem(int position) {
        return coverList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        View grid = null;
        convertView = null;

        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            grid = new View(mContext);

            grid = inflater.inflate(R.layout.item_category_row, parent, false);
            ImageView mCatIv = (ImageView) grid.findViewById(R.id.item_cat_img);
            TextView mCatNameTv = (TextView) grid.findViewById(R.id.item_cat_name_tv);
            Typeface typeFace=Typeface.createFromAsset(mContext.getAssets(),"fonts/Montserrat_Medium.otf");
            mCatNameTv.setTypeface(typeFace);
            Picasso.with(mContext)
                    .load(coverList.get(position).getCatImage())
                    .placeholder(R.drawable.fullbox)
                    .fit()
                    .centerCrop()
                    .into(mCatIv);

            mCatNameTv.setText(coverList.get(position).getCatName());


        }

        return grid;
    }

}
