package com.leonates.model;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.leonates.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


/**
 * Created by Pelicans on 3/18/2016.
 */
public class ProductAdapter extends BaseAdapter {


    private Context mContext;

    private ArrayList<ProductModel> coverList1;


    public ProductAdapter(Context c, ArrayList<ProductModel> coverList) {
        mContext = c;
        this.coverList1 = coverList;


    }

    @Override
    public int getCount() {
        return coverList1.size();
    }

    @Override
    public ProductModel getItem(int position) {
        return coverList1.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        View grid = null;
        convertView = null;

        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            grid = new View(mContext);

            grid = inflater.inflate(R.layout.item_products_row, parent, false);
            ImageView mProdIv = (ImageView) grid.findViewById(R.id.item_prod_list_img);


            Picasso.with(mContext)
                    .load(coverList1.get(position).getImage1())
                    .placeholder(R.drawable.half_box)
                    .fit()
                    .centerCrop()
                    .into(mProdIv);


        }

        return grid;
    }

}
