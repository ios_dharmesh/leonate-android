package com.leonates;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.leonates.adapter.TipsAdapter;
import com.leonates.library.ConnectionDetector;
import com.leonates.library.Constant;
import com.leonates.library.Pref;
import com.leonates.model.TipsModel;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Created by sachin on 7/11/2017.
 */

public class ActivityTips extends AppCompatActivity {
    Toolbar toolbar;
    GridView mList;

    ArrayList<TipsModel> mTipsList = new ArrayList<TipsModel>();
    TipsAdapter mTipsAdapter;
    ConnectionDetector cd;
    ProgressDialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tips_list);
        toolbar = (Toolbar) findViewById(R.id.tips_list_toolbar);
        if (toolbar != null) {

            ImageView imgBack = (ImageView) toolbar.findViewById(R.id.tips_list_back_iv);
            TextView title = (TextView) toolbar.findViewById(R.id.tips_list_title);
            Typeface typeFace = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Montserrat_Medium.otf");
            title.setTypeface(typeFace);
            imgBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

            setSupportActionBar(toolbar);
        }
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mList = (GridView) findViewById(R.id.tips_list_gv);

        cd = new ConnectionDetector(ActivityTips.this);
        if (cd.isConnectingToInternet()) {
            getTipsList();
        } else {
            Toast.makeText(ActivityTips.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(mTipsList.get(i).getVideoLink())));
            }
        });

    }

    private void getTipsList() {
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();
        AsyncHttpClient client = new AsyncHttpClient();
        try {
            client.setTimeout(40 * 1000);

            client.setMaxRetriesAndTimeout(2, 40 * 1000);
            RequestParams jsonParams = new RequestParams();
            jsonParams.put("apiKey", "w2e2rosrt3y5u6iterrest8iahug4h581e3");
            jsonParams.put("deviceType", "android");
            jsonParams.put("deviceToken", Pref.getValue(ActivityTips.this, Constant.PREF_DEVICE_ID, ""));
            jsonParams.put("intUdId", "zsdffsdf");
            jsonParams.put("apiKey", "w2e2rosrt3y5u6iterrest8iahug4h581e3");
            jsonParams.put("userId", Pref.getValue(ActivityTips.this, Constant.PREF_USER_ID, ""));


            client.post(ActivityTips.this, Constant.server_path + "API/VideoList", jsonParams, new JsonHttpResponseHandler() {


                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    Log.e("getTipsList", "getTipsList" + response.toString());
                    String status = response.optString("status");
                    String errorcode = response.optString("errorcode");

                    if (errorcode.equalsIgnoreCase("-1")) {

                        String devid = Pref.getValue(ActivityTips.this, Constant.PREF_DEVICE_ID, "");
                        Pref.deletAll(ActivityTips.this);
                        Pref.setValue(ActivityTips.this, Constant.PREF_DEVICE_ID, devid);
                        Intent logout = new Intent(ActivityTips.this, RegistrationActivity.class);
                        logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(logout);
                        finish();
                    } else {
                        if (status.equalsIgnoreCase("True")) {
                            if (mTipsList.size() > 0) {
                                mTipsList.clear();
                            }


                            try {
                                JSONArray NotiArray = response.optJSONArray("data");
                                for (int i = 0; i < NotiArray.length(); i++) {
                                    JSONObject notiObject = NotiArray.optJSONObject(i);
                                    String videosId = notiObject.optString("videoId");
                                    String videoLink = notiObject.optString("videoLink");
                                    String createdDate = notiObject.optString("createdDate");

                                    mTipsList.add(new TipsModel(videosId, videoLink, createdDate));
                                }
                                mTipsAdapter = new TipsAdapter(ActivityTips.this, mTipsList);
                                mList.setAdapter(mTipsAdapter);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else if (status.equalsIgnoreCase("failed")) {
                            String message = response.optString("message");
                            Toast.makeText(ActivityTips.this, message, Toast.LENGTH_LONG).show();
                        }
                    }
                }

                @Override
                public void onFinish() {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    Toast.makeText(ActivityTips.this, "Something went wrong!!!Please try again.", Toast.LENGTH_LONG).show();

                }


                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Log.e("onFailure,", "" + responseString);
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    Toast.makeText(ActivityTips.this, "Something went wrong!!!Please try again.", Toast.LENGTH_LONG).show();

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
