package com.leonates;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by sachin on 6/24/2017.
 */

public class ActivityTools extends AppCompatActivity {


    RelativeLayout mAreaCalcRl, mLengthCalcRl, mCompassRl, mCalculatorRL, mWeatherRl;
    TextView mAreaHint, mLengthHint, mCompassHint, mWeatherhint, mCalcHint;
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tools);

        toolbar = (Toolbar) findViewById(R.id.tools_toolbar);
        if (toolbar != null) {

            ImageView imgBack = (ImageView) toolbar.findViewById(R.id.tools_back_iv);
            TextView title = (TextView) toolbar.findViewById(R.id.tools_title);
            Typeface typeFace = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Montserrat_Medium.otf");
            title.setTypeface(typeFace);
            imgBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

            setSupportActionBar(toolbar);
        }
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        mAreaCalcRl = (RelativeLayout) findViewById(R.id.tools_area_rl);
        mLengthCalcRl = (RelativeLayout) findViewById(R.id.tools_length_rl);
        mCompassRl = (RelativeLayout) findViewById(R.id.tools_compass_rl);
        mCalculatorRL = (RelativeLayout) findViewById(R.id.tools_calc_rl);
        mWeatherRl = (RelativeLayout) findViewById(R.id.tools_weather_rl);

        mAreaHint = (TextView) findViewById(R.id.tools_area_hint_tv);
        mLengthHint = (TextView) findViewById(R.id.tools_length_hint_tv);
        mCompassHint = (TextView) findViewById(R.id.tools_compass_hint_tv);
        mWeatherhint = (TextView) findViewById(R.id.tools_calc_hint_tv);
        mCalcHint = (TextView) findViewById(R.id.tools_weather_hint_tv);

        Typeface typeFace = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Montserrat_Bold.otf");
        mAreaHint.setTypeface(typeFace);
        mLengthHint.setTypeface(typeFace);
        mCompassHint.setTypeface(typeFace);
        mWeatherhint.setTypeface(typeFace);
        mCalcHint.setTypeface(typeFace);

        mAreaCalcRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ActivityTools.this, AreaConverterActivity.class);
                startActivity(i);
            }
        });
        mLengthCalcRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ActivityTools.this, LengthConverterActivity.class);
                startActivity(i);
            }
        });
        mCompassRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ActivityTools.this, CompassActivity.class);
                startActivity(i);
            }
        });
        mCalculatorRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ActivityTools.this, CalculatorActivity.class);
                startActivity(i);
            }
        });
        mWeatherRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ActivityTools.this, ActivityWeather.class);
                startActivity(i);
            }
        });
    }
}
