package com.leonates;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.leonates.library.ConnectionDetector;
import com.leonates.library.Constant;
import com.leonates.library.Pref;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Pelicans on 6/26/2017.
 */

public class ActivityScan extends AppCompatActivity implements View.OnClickListener {
    private Button buttonScan;
    Toolbar toolbar;

    //qr code scanner object
    private IntentIntegrator qrScan;
    ProgressDialog dialog;
    private static final int REQUEST_CAMERA = 112;
    private static final String[] PERMS_TAKE_PICTURE = {
            Manifest.permission.CAMERA

    };
    ConnectionDetector cd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        toolbar = (Toolbar) findViewById(R.id.scan_toolbar);
        if (toolbar != null) {

            ImageView imgBack = (ImageView) toolbar.findViewById(R.id.scan_back_iv);
            TextView title = (TextView) toolbar.findViewById(R.id.scan_title);
            Typeface typeFace = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Montserrat_Medium.otf");
            title.setTypeface(typeFace);
            imgBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

            setSupportActionBar(toolbar);
        }
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        buttonScan = (Button) findViewById(R.id.buttonScan);
        Typeface typeFace = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Montserrat_Medium.otf");
        buttonScan.setTypeface(typeFace);
        cd = new ConnectionDetector(ActivityScan.this);
        //intializing scan object
        qrScan = new IntentIntegrator(this);
        buttonScan.setOnClickListener(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CAMERA: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // selectImage();
                    checkPermission();

                } else {
                    Toast.makeText(ActivityScan.this, "The app was not allowed to write to your storage. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show();
                }
            }
        }

    }

    public void checkPermission() {
        boolean hasPermission = (ContextCompat.checkSelfPermission(ActivityScan.this,
                Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED);

        if (!hasPermission) {
            ActivityCompat.requestPermissions(ActivityScan.this, PERMS_TAKE_PICTURE,
                    REQUEST_CAMERA);
        } else {

            qrScan.initiateScan();

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            //if qrcode has nothing in it
            if (result.getContents() == null) {
                Toast.makeText(this, "Result Not Found", Toast.LENGTH_LONG).show();
            } else {
                //if qr contains data
                try {

                    //converting the data to json
                    //JSONObject obj = new JSONObject(result.getContents());
                    //setting values to textviews
                    if (cd.isConnectingToInternet()) {
                        prodScanDetails(result.getContents());
                    } else {
                        Toast.makeText(ActivityScan.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    //if control comes here
                    //that means the encoded format not matches
                    //in this case you can display whatever data is available on the qrcode
                    //to a toast
                    Toast.makeText(this, result.getContents(), Toast.LENGTH_LONG).show();
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    @Override
    public void onClick(View view) {
        checkPermission();

    }

    private void getUserPoints(final String message) {

        AsyncHttpClient client = new AsyncHttpClient();
        try {
            client.setTimeout(40 * 1000);

            client.setMaxRetriesAndTimeout(2, 40 * 1000);
            RequestParams jsonParams = new RequestParams();
            jsonParams.put("apiKey", "w2e2rosrt3y5u6iterrest8iahug4h581e3");
            jsonParams.put("deviceType", "android");
            jsonParams.put("deviceToken", Pref.getValue(ActivityScan.this, Constant.PREF_DEVICE_ID, ""));
            jsonParams.put("intUdId", "zsdffsdf");
            jsonParams.put("apiKey", "w2e2rosrt3y5u6iterrest8iahug4h581e3");
            jsonParams.put("userId", Pref.getValue(ActivityScan.this, Constant.PREF_USER_ID, ""));


            client.post(ActivityScan.this, Constant.server_path + "API/UserPoints", jsonParams, new JsonHttpResponseHandler() {


                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    try {
                        Log.e("getCategoryList", "getCategoryList" + response.toString());
                        String status = response.optString("status");
                        String errorcode = response.optString("errorcode");

                        if (errorcode.equalsIgnoreCase("-1")) {

                            String devid = Pref.getValue(ActivityScan.this, Constant.PREF_DEVICE_ID, "");
                            Pref.deletAll(ActivityScan.this);
                            Pref.setValue(ActivityScan.this, Constant.PREF_DEVICE_ID, devid);
                            Intent logout = new Intent(ActivityScan.this, RegistrationActivity.class);
                            logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(logout);
                            finish();
                        } else {
                            if (status.equalsIgnoreCase("True")) {


                                try {
                                    String totalPoints = response.optString("totalPoints");
                                    String lastMonth = response.optString("lastMonth");
                                    Pref.setValue(ActivityScan.this, Constant.PREF_TOTAL_POINTS, totalPoints);

                                    Pref.setValue(ActivityScan.this, Constant.PREF_LAST_POINTS, lastMonth);

                                    DashBoardActivity.mTotalPoints.setText(Pref.getValue(ActivityScan.this, Constant.PREF_TOTAL_POINTS, "0"));
                                    DashBoardActivity.mLastPoints.setText(Pref.getValue(ActivityScan.this, Constant.PREF_LAST_POINTS, "0"));

                                    Toast.makeText(ActivityScan.this, message, Toast.LENGTH_LONG).show();

                                    finish();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            } else if (status.equalsIgnoreCase("failed")) {
                                String message = response.optString("message");
                                Toast.makeText(ActivityScan.this, message, Toast.LENGTH_LONG).show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFinish() {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    //  Toast.makeText(ActivityScan.this, "Something went wrong!!!Please try again.", Toast.LENGTH_LONG).show();

                }


                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Log.e("onFailure,", "" + responseString);
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    //  Toast.makeText(ActivityScan.this, "Something went wrong!!!Please try again.", Toast.LENGTH_LONG).show();

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void prodScanDetails(String id) {
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();
        AsyncHttpClient client = new AsyncHttpClient();
        try {


            RequestParams jsonParams = new RequestParams();

            jsonParams.put("userId", Pref.getValue(ActivityScan.this, Constant.PREF_USER_ID, ""));
            jsonParams.put("product_to_qr_Id", id);
            jsonParams.put("deviceType", "android");
            jsonParams.put("deviceToken", Pref.getValue(ActivityScan.this, Constant.PREF_DEVICE_ID, ""));
            jsonParams.put("intUdId", "zsdffsdf");
            jsonParams.put("apiKey", "w2e2rosrt3y5u6iterrest8iahug4h581e3");
            Log.e("jsonParams", "jsonParams" + jsonParams.toString());
            client.setTimeout(40 * 1000);

            client.setMaxRetriesAndTimeout(2, 40 * 1000);
            client.post(ActivityScan.this, Constant.server_path + "API/QrScan", jsonParams, new JsonHttpResponseHandler() {


                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);

                    Log.e("xsvhkdsv", "sdkjnfhvcds" + response.toString());
                    String status = response.optString("status");
                    String errorcode = response.optString("errorcode");

                    if (errorcode.equalsIgnoreCase("-1")) {

                        String devid = Pref.getValue(ActivityScan.this, Constant.PREF_DEVICE_ID, "");
                        Pref.deletAll(ActivityScan.this);
                        Pref.setValue(ActivityScan.this, Constant.PREF_DEVICE_ID, devid);
                        Intent logout = new Intent(ActivityScan.this, RegistrationActivity.class);
                        logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(logout);
                        finish();
                    } else {
                        if (status.equalsIgnoreCase("True")) {
                            try {
                                String message = response.optString("msg");
                                getUserPoints(message);
                                // Toast.makeText(ActivityScan.this, message, Toast.LENGTH_LONG).show();

                                //  finish();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        } else if (status.equalsIgnoreCase("failed")) {
                            if (dialog != null && dialog.isShowing()) {
                                dialog.dismiss();
                            }
                            String message = response.optString("msg");
                            Toast.makeText(ActivityScan.this, message, Toast.LENGTH_LONG).show();
                        }
                    }
                }

                @Override
                public void onFinish() {

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    Toast.makeText(ActivityScan.this, "Something went wrong!!!Please try again.", Toast.LENGTH_LONG).show();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Log.e("onFailure,", "" + responseString);
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


}
