package com.leonates;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.leonates.adapter.MyAdapter;
import com.leonates.library.ConnectionDetector;
import com.viven.imagezoom.ImageZoomHelper;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import cz.msebera.android.httpclient.util.ByteArrayBuffer;


/**
 * Created by sachin on 7/11/2017.
 */

public class SliderImageActivity extends AppCompatActivity {

    private static ViewPager mPager;

    String catName = "";
    TextView mShareTxt, mSaveTxt;
    String url;
    int pos = 0;
    MyAdapter mMyAdapter;
    ProgressDialog dialog;
    ConnectionDetector cd;
    ImageZoomHelper imageZoomHelper;
    private static final int REQUEST_WRITE_STORAGE = 311;
    private static final String[] PERMS_TAKE_PICTURE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    boolean isShare = false;
    File file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        if (getIntent().getExtras() != null) {


            catName = intent.getStringExtra("catName");
            pos = intent.getIntExtra("pos", 0);
        }

        setContentView(R.layout.activity_image_slider);
        init();
    }

    private void init() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.slide_toolbar);
        if (toolbar != null) {
            TextView mTiotle = (TextView) toolbar.findViewById(R.id.slide_title);
            mTiotle.setText(catName);
            Typeface typeFace = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Montserrat_Medium.otf");
            mTiotle.setTypeface(typeFace);
            ImageView imgBack = (ImageView) toolbar.findViewById(R.id.slide_back_iv);
            imgBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

            setSupportActionBar(toolbar);

        }
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mPager = (ViewPager) findViewById(R.id.pager);
        mShareTxt = (TextView) findViewById(R.id.slide_share);
        mSaveTxt = (TextView) findViewById(R.id.slide_save);



        Typeface typeFace = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Montserrat_Medium.otf");
        mShareTxt.setTypeface(typeFace);
        mSaveTxt.setTypeface(typeFace);

        cd = new ConnectionDetector(SliderImageActivity.this);
        imageZoomHelper=new ImageZoomHelper(SliderImageActivity.this);
        Log.e("dfsfsfsdf", "dfvsf" + ActivityProductList.mProductList.size());
        mMyAdapter = new MyAdapter(SliderImageActivity.this, ActivityProductList.mProductList);
        mPager.setAdapter(mMyAdapter);

        mPager.setCurrentItem(pos);
        url = ActivityProductList.mProductList.get(pos).getImage1();

        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                url = ActivityProductList.mProductList.get(i).getImage1();
                Log.e("dfsfsf", "dfsfsdf" + url);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        mSaveTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isShare = false;
                checkPermission();

            }
        });
        mShareTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isShare = true;
                checkPermission();


            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_WRITE_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // selectImage();
                    checkPermission();

                } else {
                    Toast.makeText(SliderImageActivity.this, "The app was not allowed to write to your storage. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show();
                }
            }
        }

    }

    public void checkPermission() {
        boolean hasPermission = (ContextCompat.checkSelfPermission(SliderImageActivity.this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(SliderImageActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);

        if (!hasPermission) {
            ActivityCompat.requestPermissions(SliderImageActivity.this, PERMS_TAKE_PICTURE,
                    REQUEST_WRITE_STORAGE);
        } else {

            if (cd.isConnectingToInternet()) {
                dialog = new ProgressDialog(this);
                dialog.setMessage("Please wait...");
                dialog.setCancelable(false);
                dialog.show();
                new getImage().execute(url);
                //DownloadFromUrl(url);
            } else {
                Toast.makeText(SliderImageActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void openScreenshot(File imageFile) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);

        intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(imageFile));
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.setType("image/*");
        startActivity(Intent.createChooser(intent, "Share Image"));
    }


    public boolean DownloadFromUrl(String imageUrl) {
        try {


            URL url = new URL(imageUrl); //you can write here any link

            final String dir = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS) + "/Leonate/";
            File newdir = new File(dir);
            if (newdir.exists()) {
            } else {
                newdir.mkdir();
            }

            String fileName = System.currentTimeMillis() + ".jpg";
            //      String path = Environment.getExternalStorageDirectory().toString();
            file = new File(dir, fileName);

            // File file = new File(fileName);


            URLConnection ucon = url.openConnection();
            InputStream is = ucon.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
                    /*
                     * Read bytes to the Buffer until there is nothing more to read(-1).
                     */
            ByteArrayBuffer baf = new ByteArrayBuffer(1024);
            int current = 0;
            while ((current = bis.read()) != -1) {
                baf.append((byte) current);
            }

            FileOutputStream fos = new FileOutputStream(file);
            fos.write(baf.toByteArray());
            fos.close();

            return true;

        } catch (IOException e) {

            e.printStackTrace();
            return false;

        }
    }

    public class getImage extends AsyncTask<String, String, Boolean> {

        boolean isDownloaded = false;

        @Override
        protected Boolean doInBackground(String... strings) {
            isDownloaded = DownloadFromUrl(strings[0]);

            return isDownloaded;
        }

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
            if (aBoolean) {
                if (isShare) {
                    openScreenshot(file);
                } else {
                    Toast.makeText(SliderImageActivity.this, "Image save successfully.", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(SliderImageActivity.this, "Please try again later.", Toast.LENGTH_SHORT).show();
            }
        }
    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return imageZoomHelper.onDispatchTouchEvent(ev) || super.dispatchTouchEvent(ev);
    }
}


