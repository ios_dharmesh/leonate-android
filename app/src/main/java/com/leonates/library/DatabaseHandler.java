package com.leonates.library;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


import com.leonates.model.GiftModel;
import com.leonates.model.ProductModel;

import java.util.ArrayList;


public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "leonate";

    // Contacts table name
    private static final String TABLE_PRODUCTLIST = "productlist";
    private static final String TABLE_GIFS = "gifts";

    String[] columns ={"id","categoryproductid", "categoryproductname","image","catid","catname","catimage"};
    String[] giftcolumns ={"id","giftsId", "points","giftImage","giftName","giftDesc"};

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table productlist " +
                        "(id integer primary key,categoryproductid text, categoryproductname text,image text,catid text, catname text,catimage text)"
        );
        db.execSQL(
                "create table gifts " +
                        "(id integer primary key,giftsId text, points text,giftImage text,giftName text, giftDesc text)"
        );

    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS productlist");
        db.execSQL("DROP TABLE IF EXISTS gifts");



        // Create tables again
        onCreate(db);
    }





    public void deleteGiftsTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_GIFS, null, null);
    }


    public void deleteProductTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_PRODUCTLIST, null, null);
    }


    public  ArrayList<ProductModel> getProductList(String owner){
        try {
            ArrayList<ProductModel> mMainArrayList = new ArrayList<ProductModel>();
            SQLiteDatabase db= this.getReadableDatabase();
           // String orderBy =  "id DESC";// "cast(createdtime as REAL) DESC";
            Cursor cursor = db.query(TABLE_PRODUCTLIST, columns, "catid=?", new String[]{owner}, null, null, null);
            Log.e("cursorrr","sdfs"+cursor.getCount());

            if (cursor.moveToFirst()) {
                do {
                    String id = cursor.getString(0);
                    String categoryproductid = cursor.getString(1);
                    String categoryproductname = cursor.getString(2);
                    String image = cursor.getString(3);
                    String catid = cursor.getString(4);
                    String catname = cursor.getString(5);
                    String catimage = cursor.getString(6);

                    mMainArrayList.add(new ProductModel(categoryproductid, categoryproductname, image, catid, catname, catimage));

                } while (cursor.moveToNext());

            }


            cursor.close();
            return mMainArrayList;
        }catch (Exception e){
            e.printStackTrace();
        }


        return null;
    }
    public  ArrayList<GiftModel> getGiftsList(){
        try {
            ArrayList<GiftModel> mMainArrayList = new ArrayList<GiftModel>();
            SQLiteDatabase db= this.getReadableDatabase();
            // String orderBy =  "id DESC";// "cast(createdtime as REAL) DESC";
            Cursor cursor = db.query(TABLE_GIFS, giftcolumns, null, null, null, null, null);
            Log.e("cursorrr","sdfs"+cursor.getCount());
            if (cursor.moveToFirst()) {
                do {
                    String id = cursor.getString(0);
                    String giftsId = cursor.getString(1);
                    String points = cursor.getString(2);
                    String giftImage = cursor.getString(3);
                    String giftName = cursor.getString(4);
                    String giftDesc = cursor.getString(5);


                    mMainArrayList.add(new GiftModel(giftsId, points, giftImage, giftName, giftDesc));

                } while (cursor.moveToNext());

            }


            cursor.close();
            return mMainArrayList;
        }catch (Exception e){
            e.printStackTrace();
        }


        return null;
    }
    public  ArrayList<GiftModel> getGiftsListPrice(String price,String price1){
        try {
            ArrayList<GiftModel> mMainArrayList = new ArrayList<GiftModel>();
            SQLiteDatabase db= this.getReadableDatabase();
            // String orderBy =  "id DESC";// "cast(createdtime as REAL) DESC";
            Cursor cursor = db.query(TABLE_GIFS, giftcolumns,"price=?"+ " BETWEEN '" +"price1=?", new String[]{price,price1}, null, null, null);

            Log.e("cursorrr","sdfs"+cursor.getCount());
            if (cursor.moveToFirst()) {
                do {
                    String id = cursor.getString(0);
                    String giftsId = cursor.getString(1);
                    String points = cursor.getString(2);
                    String giftImage = cursor.getString(3);
                    String giftName = cursor.getString(4);
                    String giftDesc = cursor.getString(5);


                    mMainArrayList.add(new GiftModel(giftsId, points, giftImage, giftName, giftDesc));

                } while (cursor.moveToNext());

            }


            cursor.close();
            return mMainArrayList;
        }catch (Exception e){
            e.printStackTrace();
        }


        return null;
    }

    public  int getProductList(){
        try {
           int count=0;
            SQLiteDatabase db= this.getReadableDatabase();
            // String orderBy =  "id DESC";// "cast(createdtime as REAL) DESC";
            Cursor cursor = db.query(TABLE_PRODUCTLIST, columns, null, null, null, null, null);
            if (cursor.moveToFirst()) {

                count=cursor.getCount();
            }


            cursor.close();
            return count;
        }catch (Exception e){
            e.printStackTrace();
        }


        return -1;
    }




}