package com.leonates.library;

/**
 * Created by Pelicans on 6/20/2017.
 */

public class convert  {
    private MetricConverter metricConverter;

    public convert()
    {
        //Can just be empty, there are no global variables
    }


    public double convert(String originalUnit, String newUnit, double input) {
        metricConverter = new MetricConverter();
        double num1 = input;
        double num2 = 0.0d;

        String original = originalUnit.toLowerCase();
        String newU = newUnit.toLowerCase();

        switch(original)
        {
            case "inches":
            {
                switch(newU)
                {
                    case "inches":
                        num2 = num1;
                        break;
                    case "feet":

                        num2 = num1 / 12.0d;
                        break;
                    case "yards":

                        num2 = num1 / 36.0d;
                        break;
                    case "miles":

                        num2 = num1 / 63360.0d;
                        break;
                    case "millimeters":

                        num2 = num1 * 25.4d;
                        break;
                    case "centimeters":

                        num2 = num1 * 2.54d;
                        break;
                    case "meters":

                        num2 = num1 * 0.0254d;
                        break;
                    case "kilometers":

                        num2 = num1 * 0.0000254d;
                        break;
                }
                break;
            }
            case "feet":
            {
                switch(newU)
                {
                    case "inches":

                        num2 = num1*12.0d;
                        break;
                    case "feet":
                        num2 = num1;
                        break;
                    case "yards":

                        num2 = num1/3.0d;
                        break;
                    case "miles":

                        num2 = num1/5280.0d;
                        break;
                    case "millimeters":

                        num2 = num1*304.8d;
                        break;
                    case "centimeters":

                        num2 = num1*30.48d;
                        break;
                    case "meters":

                        num2 = num1*0.3048d;
                        break;
                    case "kilometers":

                        num2 = num1*0.0003048d;
                        break;
                }
                break;
            }
            case "yards":
            {
                switch(newU) {
                    case "inches":

                        num2 = num1 * 36.0d;
                        break;
                    case "feet":

                        num2 = num1 * 3.0d;
                        break;
                    case "yards":
                        num2 = num1;
                        break;
                    case "miles":

                        num2 = num1 / 1760.0d;
                        break;
                    case "millimeters":

                        num2 = num1 * 914.4d;
                        break;
                    case "centimeters":

                        num2 = num1*91.44d;
                        break;
                    case "meters":
                        num2 = num1*0.9144d;
                        break;
                    case "kilometers":

                        num2 = num1/1093.61d;
                        break;
                }
                break;
            }
            case "miles":
            {
                switch(newU)
                {
                    case "inches":

                        num2 = num1*6330.0d;
                        break;
                    case "feet":

                        num2 = num1*5280.0d;
                        break;
                    case "yards":

                        num2 = num1*1760.0d;
                        break;
                    case "miles":
                        num2 = num1;
                        break;
                    case "millimeters":

                        num2 = num1*1609340.0d;
                        break;
                    case "centimeters":

                        num2 = num1*160934.0d;
                        break;
                    case "meters":

                        num2 = num1*1609.34d;
                        break;
                    case "kilometers":

                        num2 = num1*1.60934d;
                        break;
                }
                break;
            }
            case "millimeters":
            {
                switch(newU)
                {
                    case "inches":
                        num2 = num1*25.4d;
                        break;
                    case "feet":
                        num2 = num1/304.8d;
                        break;
                    case "yards":
                        num2 = num1/914.4d;
                        break;
                    case "miles":
                        num2 = num1/1609000.0d;
                        break;
                    case "millimeters":
                        num2 = num1;
                        break;
                    case "centimeters":
                        num2 = metricConverter.metricConvert(num1, "milli", "centi");
                        break;
                    case "meters":
                        num2 = metricConverter.metricConvert(num1, "milli", "unit");
                        break;
                    case "kilometers":
                        num2 = metricConverter.metricConvert(num1, "milli", "kilo");
                        break;
                }
                break;
            }

            case "centimeters":
            {
                switch(newU)
                {
                    case "inches":
                        num2 = num1 / 2.54d;
                        break;
                    case "feet":
                        num2 = num1 / 30.48d;
                        break;
                    case "yards":
                        num2 = num1 / 91.44d;
                        break;
                    case "miles":
                        num2 = num1/160934.0d;
                        break;
                    case "millimeters":
                        num2 = metricConverter.metricConvert(num1, "centi", "milli");
                        break;
                    case "centimeters":
                        num2 = num1;
                        break;
                    case "meters":
                        num2 = metricConverter.metricConvert(num1, "centi", "unit");
                        break;
                    case "kilometers":
                        num2 = metricConverter.metricConvert(num1, "centi", "kilo");
                        break;
                }
                break;
            }
            case "meters":
            {
                switch(newU) {
                    case "inches":
                        num2 = num1 * 39.3701d;
                        break;
                    case "feet":
                        num2 = num1 * 3.28084d;
                        break;
                    case "yards":
                        num2 = num1*1.09361d;
                        break;
                    case "miles":
                        num2 = num1/1609.34d;
                        break;
                    case "millimeters":
                        num2 = metricConverter.metricConvert(num1, "unit", "milli");
                        break;
                    case "centimeters":
                        num2 = metricConverter.metricConvert(num1, "unit", "centi");
                        break;
                    case "meters":
                        num2 = num1;
                        break;
                    case "kilometers":
                        num2 = metricConverter.metricConvert(num1, "unit", "kilo");
                        break;
                }
                break;
            }
            case "kilometers":
            {
                switch(newU)
                {
                    case "inches":
                        num2 = num1*39370.1d;
                        break;
                    case "feet":
                        num2 = num1*3280.84d;
                        break;
                    case "yards":
                        num2 = num1*1093.61d;
                        break;
                    case "miles":
                        num2 = num1/1.60934d;
                        break;
                    case "millimeters":
                        num2 = metricConverter.metricConvert(num1, "kilo", "milli");
                        break;
                    case "centimeters":
                        num2 = metricConverter.metricConvert(num1, "kilo", "centi");
                        break;
                    case "meters":
                        num2 = metricConverter.metricConvert(num1, "kilo", "unit");
                        break;
                    case "kilometers":
                        num2 = num1;
                        break;
                }
                break;
            }
        }

        return num2;
    }


    public double areaConvert(String originalUnit, String newUnit,double originalNumber)
    { //Begin convertArea
        //Make two doubles, one that holds the original and one that will be redefined where needed
        double num1 = originalNumber;
        double num2 = 0.0d;

        //Make two strings, capturing the units fed to the method
        String originalU = originalUnit.toLowerCase();
        String newU = newUnit.toLowerCase();

        switch(originalU)
        {
            //Begin unit conversions
            case "square inches":
                switch(newU)
                { //Begin converting from square inches
                    case "square inches":
                        num2 = num1;
                        break;
                    case "square feet":
                        num2 = num1/144.0d;
                        break;
                    case "square yards":
                        num2 = num1/1296.0d;
                        break;
                    case "square miles":
                        num2 = num1/4014000000.0d;
                        break;
                    case "acres":
                        num2 = num1/6273000.0d;
                        break;
                    case "square kilometers":
                        num2 = num1/1550000000.0d;
                        break;
                    case "square meters":
                        num2 = num1/1550.0d;
                        break;
                } //End converting from square inches
                break;
            case "square feet":
                switch(newU)
                { //Begin converting from square feet
                    case "square inches":
                        num2 = num1*144.0d;
                        break;
                    case "square feet":
                        num2 = num1;
                        break;
                    case "square yards":
                        num2 = num1/9.0d;
                        break;
                    case "square miles":
                        num2 = num1/27880000.0d;
                        break;
                    case "acres":
                        num2 = num1/43560.0d;
                        break;
                    case "square kilometers":
                        num2 = num1/10760000.0d;
                        break;
                    case "square meters":
                        num2 = num1/10.7639d;
                        break;
                } //End converting from square feet
                break;
            case "square yards":
                switch(newU)
                { //Begin converting from square yards
                    case "square inches":
                        num2 = num1*1296.0d;
                        break;
                    case "square feet":
                        num2 = num1*9.0d;
                        break;
                    case "square yards":
                        num2 = num1;
                        break;
                    case "square miles":
                        num2 = num1/3098000.0d;
                        break;
                    case "acres":
                        num2 = num1/4840.0d;
                        break;
                    case "square kilometers":
                        num2 = num1/1196000.0d;
                        break;
                    case "square meters":
                        num2 = num1/1.19599d;
                        break;
                }//End converting from square yards
                break;
            case "square miles":
                switch(newU)
                { //Begin converting from square miles
                    case "square inches":
                        num2 = num1*4014000000.0d;
                        break;
                    case "square feet":
                        num2 = num1*27880000.0d;
                        break;
                    case "square yards":
                        num2 = num1*3098000.0d;
                        break;
                    case "square miles":
                        num2 = num1;
                        break;
                    case "acres":
                        num2 = num1*640.0d;
                        break;
                    case "square kilometers":
                        num2 = num1*2.58999d;
                        break;
                    case "square meters":
                        num2 = num1*2590000.0d;
                        break;
                }//End converting from square miles
                break;
            case "acres":
                switch(newU)
                {//Begin converting from acres
                    case "square inches":
                        num2 = num1*6273000.0d;
                        break;
                    case "square feet":
                        num2 = num1*43560.0d;
                        break;
                    case "square yards":
                        num2 = num1*4840.0d;
                        break;
                    case "square miles":
                        num2 = num1/640.0d;
                        break;
                    case "square acres":
                        num2 = num1;
                        break;
                    case "square kilometers":
                        num2 = num1/247.105d;
                        break;
                    case "square meters":
                        num2 = num1*4046.86d;
                        break;
                } //End converting from acres
                break;
            case "kilo":
                switch(newU)
                { //Begin converting from square kilometers
                    case "square inches":
                        num2 = num1*1550000000.0d;
                        break;
                    case "square feet":
                        num2 = num1*10760000.0d;
                        break;
                    case "square yards":
                        num2 = num1*1196000.0d;
                        break;
                    case "square miles":
                        num2 = num1/2.58999d;
                        break;
                    case "acres":
                        num2 = num1;
                        break;
                    case "square kilometers":
                        num2 = num1;
                        break;
                    case "square meters":
                        num2 = num1*1000000.0d;
                        break;
                } //End converting from square kilometers
                break;
            case "square meters":
                switch(newU)
                { //Begin converting from square meters
                    case "square inches":
                        num2 = num1*1550.0d;
                        break;
                    case "square feet":
                        num2 = num1*10.7639d;
                        break;
                    case "square yards":
                        num2 = num1*1.19599d;
                        break;
                    case "square miles":
                        num2 = num1/2590000.0d;
                        break;
                    case "acres":
                        num2 = num1/4046.86d;
                        break;
                    case "square kilometers":
                        num2 = num1/1000000.0d;
                        break;
                    case "square meters":
                        num2 = num1;
                        break;
                } //End converting from square meters
        } //End conversion table

        //Return the resulting number from the conversion table above
        return num2;
    }
}