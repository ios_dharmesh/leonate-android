package com.leonates.library;

/**
 * Created by Pelicans on 3/2/2016.
 */
public class Constant {

   public static String server_path="http://vidhisoft.com/dev/leonate/";


    public static final String PREF_FILE = "MyPrefs" ;
    public static final String PREF_USER_ID = "id" ;
    public static final String PREF_USER_LNAME = "lname" ;
 public static final String PREF_USER_FNAME = "fname" ;
    public static final String PREF_USER_EMAIL = "email" ;
    public static final String PREF_USER_NO = "num" ;
    public static final String PREF_USER_IMAGE = "image" ;
    public static final String PREF_IS_LOGIN = "login" ;
    public static final String PREF_TEMP_IMG = "vvv" ;
    public static final String PREF_DEVICE_ID = "devid" ;
    public static final String PREF_LANG = "lang" ;
    public static final String PREF_TOTAL_POINTS = "tpts" ;
    public static final String PREF_LAST_POINTS= "lpts" ;












    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    public static final String SHARED_PREF = "ah_firebase";

}
