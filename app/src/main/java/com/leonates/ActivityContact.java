package com.leonates;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.leonates.library.ConnectionDetector;
import com.leonates.library.Constant;

/**
 * Created by Pelicans on 7/11/2017.
 */

public class ActivityContact extends AppCompatActivity {

    WebView mContactWv;
    Toolbar toolbar;
    ConnectionDetector cd;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactus);
        toolbar = (Toolbar) findViewById(R.id.contus_toolbar);
        if (toolbar != null) {

            ImageView imgBack = (ImageView) toolbar.findViewById(R.id.contus_back_iv);
            TextView title = (TextView) toolbar.findViewById(R.id.contus_title);
            Typeface typeFace = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Montserrat_Medium.otf");
            title.setTypeface(typeFace);
            imgBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

            setSupportActionBar(toolbar);
        }
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        cd = new ConnectionDetector(ActivityContact.this);
        mContactWv=(WebView)findViewById(R.id.contus_webview);
        mContactWv.setWebViewClient(new MyBrowser());
        mContactWv.getSettings().setLoadsImagesAutomatically(true);

        mContactWv.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        mContactWv.loadUrl( Constant.server_path+"uploads/contact_us.html");
    }


    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            return super.shouldOverrideUrlLoading(view, request);
        }
    }
}
