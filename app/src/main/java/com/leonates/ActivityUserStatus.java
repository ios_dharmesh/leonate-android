package com.leonates;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import com.leonates.library.ConnectionDetector;
import com.leonates.library.Constant;
import com.leonates.library.Pref;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Pelicans on 6/26/2017.
 */

public class ActivityUserStatus extends AppCompatActivity {


    TextView mSubmitTv;
    WebView mWebView;
    ConnectionDetector cd;
    Dialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_status);


        mSubmitTv = (TextView) findViewById(R.id.status_submit_btn);
        mWebView = (WebView) findViewById(R.id.status_webview);
        Typeface typeFace = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Montserrat_Medium.otf");
        mSubmitTv.setTypeface(typeFace);
        cd = new ConnectionDetector(ActivityUserStatus.this);
        mWebView.setBackgroundColor(Color.TRANSPARENT);
        mWebView.setWebViewClient(new ActivityUserStatus.MyBrowser());
        mWebView.getSettings().setLoadsImagesAutomatically(true);

        mWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        mWebView.loadUrl(Constant.server_path + "uploads/contact_us.html");
        if (cd.isConnectingToInternet()) {
            getUserStatus();
        } else {
            Toast.makeText(ActivityUserStatus.this, "No Internet connection.", Toast.LENGTH_LONG).show();
        }
        mSubmitTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cd.isConnectingToInternet()) {
                    getUserStatus();
                } else {
                    Toast.makeText(ActivityUserStatus.this, "No Internet connection.", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            return super.shouldOverrideUrlLoading(view, request);
        }
    }

    private void getUserStatus() {

        AsyncHttpClient client = new AsyncHttpClient();
        try {
            client.setTimeout(40 * 1000);

            client.setMaxRetriesAndTimeout(2, 40 * 1000);
            RequestParams jsonParams = new RequestParams();
            jsonParams.put("apiKey", "w2e2rosrt3y5u6iterrest8iahug4h581e3");
            jsonParams.put("deviceType", "android");
            jsonParams.put("deviceToken", Pref.getValue(ActivityUserStatus.this, Constant.PREF_DEVICE_ID, ""));
            jsonParams.put("intUdId", "zsdffsdf");
            jsonParams.put("apiKey", "w2e2rosrt3y5u6iterrest8iahug4h581e3");
            jsonParams.put("userId", Pref.getValue(ActivityUserStatus.this, Constant.PREF_USER_ID, ""));


            client.post(ActivityUserStatus.this, Constant.server_path + "API/CheckUserStatus", jsonParams, new JsonHttpResponseHandler() {


                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    Log.e("getCategoryList", "getCategoryList" + response.toString());
                    String status = response.optString("status");
                    if (status.equalsIgnoreCase("True")) {


                    } else if (status.equalsIgnoreCase("failed")) {
                        String message = response.optString("message");
                        Toast.makeText(ActivityUserStatus.this, message, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFinish() {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.hide();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    if (dialog != null && dialog.isShowing()) {
                        dialog.hide();
                    }
                    //  Toast.makeText(ActivityScan.this, "Something went wrong!!!Please try again.", Toast.LENGTH_LONG).show();

                }


                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Log.e("onFailure,", "" + responseString);
                    if (dialog != null && dialog.isShowing()) {
                        dialog.hide();
                    }
                    //  Toast.makeText(ActivityScan.this, "Something went wrong!!!Please try again.", Toast.LENGTH_LONG).show();

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


}
