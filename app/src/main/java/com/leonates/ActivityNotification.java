package com.leonates;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.leonates.adapter.NotificationAdapter;
import com.leonates.library.ConnectionDetector;
import com.leonates.library.Constant;
import com.leonates.library.Pref;
import com.leonates.model.NotificationModel;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Created by sachin on 7/11/2017.
 */

public class ActivityNotification extends AppCompatActivity {
    Toolbar toolbar;
    ListView mList;
    ProgressDialog dialog;
    ArrayList<NotificationModel> mNotiList = new ArrayList<NotificationModel>();
    NotificationAdapter mNotificationAdapter;
    ConnectionDetector cd;
    String flag = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        if (getIntent().getExtras() != null) {

            flag = intent.getStringExtra("flag");

        }
        setContentView(R.layout.activity_notification);
        toolbar = (Toolbar) findViewById(R.id.noti_toolbar);
        if (toolbar != null) {

            ImageView imgBack = (ImageView) toolbar.findViewById(R.id.noti_back_iv);
            TextView title = (TextView) toolbar.findViewById(R.id.noti_title);
            Typeface typeFace = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/Montserrat_Medium.otf");
            title.setTypeface(typeFace);
            imgBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (flag.equalsIgnoreCase("noti")) {
                        Intent i = new Intent(ActivityNotification.this, DashBoardActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                    } else {
                        finish();
                    }
                }
            });

            setSupportActionBar(toolbar);
        }
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mList = (ListView) findViewById(R.id.noti_lst);

        cd = new ConnectionDetector(ActivityNotification.this);
        if (cd.isConnectingToInternet()) {
            getNotificationList();
        } else {
            Toast.makeText(ActivityNotification.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }

    private void getNotificationList() {
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();
        AsyncHttpClient client = new AsyncHttpClient();
        try {
            client.setTimeout(40 * 1000);

            client.setMaxRetriesAndTimeout(2, 40 * 1000);
            RequestParams jsonParams = new RequestParams();
            jsonParams.put("apiKey", "w2e2rosrt3y5u6iterrest8iahug4h581e3");
            jsonParams.put("deviceType", "android");
            jsonParams.put("deviceToken", Pref.getValue(ActivityNotification.this, Constant.PREF_DEVICE_ID, ""));
            jsonParams.put("intUdId", "zsdffsdf");
            jsonParams.put("apiKey", "w2e2rosrt3y5u6iterrest8iahug4h581e3");
            jsonParams.put("userId", Pref.getValue(ActivityNotification.this, Constant.PREF_USER_ID, ""));


            client.post(ActivityNotification.this, Constant.server_path + "API/Notification", jsonParams, new JsonHttpResponseHandler() {


                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    Log.e("getCategoryList", "getCategoryList" + response.toString());
                    String status = response.optString("status");

                    String errorcode = response.optString("errorcode");

                    if (errorcode.equalsIgnoreCase("-1")) {

                        String devid = Pref.getValue(ActivityNotification.this, Constant.PREF_DEVICE_ID, "");
                        Pref.deletAll(ActivityNotification.this);
                        Pref.setValue(ActivityNotification.this, Constant.PREF_DEVICE_ID, devid);
                        Intent logout = new Intent(ActivityNotification.this, RegistrationActivity.class);
                        logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(logout);
                        finish();
                    } else {
                        if (status.equalsIgnoreCase("True")) {
                            if (mNotiList.size() > 0) {
                                mNotiList.clear();
                            }


                            try {
                                JSONArray NotiArray = response.optJSONArray("data");
                                for (int i = 0; i < NotiArray.length(); i++) {
                                    JSONObject notiObject = NotiArray.optJSONObject(i);
                                    String notificationId = notiObject.optString("notificationId");
                                    String notificationText = notiObject.optString("notificationText");
                                    String sentDate = notiObject.optString("sentDate");
                                    mNotiList.add(new NotificationModel(notificationId, notificationText, sentDate));
                                }
                                mNotificationAdapter = new NotificationAdapter(ActivityNotification.this, mNotiList);
                                mList.setAdapter(mNotificationAdapter);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else if (status.equalsIgnoreCase("failed")) {
                            String message = response.optString("message");
                            Toast.makeText(ActivityNotification.this, message, Toast.LENGTH_LONG).show();
                        }
                    }
                }

                @Override
                public void onFinish() {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    Toast.makeText(ActivityNotification.this, "Something went wrong!!!Please try again.", Toast.LENGTH_LONG).show();

                }


                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Log.e("onFailure,", "" + responseString);
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    Toast.makeText(ActivityNotification.this, "Something went wrong!!!Please try again.", Toast.LENGTH_LONG).show();

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onBackPressed() {
        if (flag.equalsIgnoreCase("noti")) {
            Intent i = new Intent(ActivityNotification.this, DashBoardActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                    Intent.FLAG_ACTIVITY_CLEAR_TASK |
                    Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        } else {
            finish();
        }

        super.onBackPressed();
    }
}
