package com.leonates.model;

/**
 * Created by sachin on 7/11/2017.
 */

public class HistoryModel {
    String redeemusersId;
    String giftsId;
    String giftImage;
    String giftName;

    public HistoryModel(String redeemusersId, String giftsId, String giftImage, String giftName) {
        this.redeemusersId = redeemusersId;
        this.giftsId = giftsId;
        this.giftImage = giftImage;
        this.giftName = giftName;
    }

    public String getRedeemusersId() {
        return redeemusersId;
    }

    public void setRedeemusersId(String redeemusersId) {
        this.redeemusersId = redeemusersId;
    }

    public String getGiftsId() {
        return giftsId;
    }

    public void setGiftsId(String giftsId) {
        this.giftsId = giftsId;
    }

    public String getGiftImage() {
        return giftImage;
    }

    public void setGiftImage(String giftImage) {
        this.giftImage = giftImage;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }
}
