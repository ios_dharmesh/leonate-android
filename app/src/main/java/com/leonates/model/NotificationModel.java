package com.leonates.model;

/**
 * Created by Pelicans on 6/20/2017.
 */

public class NotificationModel {

    String catId;
    String notiText;
    String notiDate;

    public NotificationModel(String catId, String notiText, String notiDate) {
        this.catId = catId;
        this.notiText = notiText;
        this.notiDate = notiDate;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getNotiText() {
        return notiText;
    }

    public void setNotiText(String notiText) {
        this.notiText = notiText;
    }

    public String getNotiDate() {
        return notiDate;
    }

    public void setNotiDate(String notiDate) {
        this.notiDate = notiDate;
    }
}
