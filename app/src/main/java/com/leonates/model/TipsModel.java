package com.leonates.model;

/**
 * Created by Pelicans on 7/15/2017.
 */

public class TipsModel {
    String videoId;
    String videoLink;
    String videoDate;

    public TipsModel(String videoId, String videoLink, String videoDate) {
        this.videoId = videoId;
        this.videoLink = videoLink;
        this.videoDate = videoDate;

    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public String getVideoLink() {
        return videoLink;
    }

    public void setVideoLink(String videoLink) {
        this.videoLink = videoLink;
    }

    public String getVideoDate() {
        return videoDate;
    }

    public void setVideoDate(String videoDate) {
        this.videoDate = videoDate;
    }
}