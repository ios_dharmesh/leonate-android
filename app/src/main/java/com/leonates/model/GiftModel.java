package com.leonates.model;

/**
 * Created by Pelicans on 7/12/2017.
 */

public class GiftModel {
    String giftsId;
    String points;
    String giftImage;
    String giftName;
    String giftDesc;


    public GiftModel(String giftsId, String points, String giftImage, String giftName,String giftDesc) {
        this.giftsId = giftsId;
        this.points = points;
        this.giftImage = giftImage;
        this.giftName = giftName;
        this.giftDesc=giftDesc;
    }

    public String getGiftsId() {
        return giftsId;
    }

    public void setGiftsId(String giftsId) {
        this.giftsId = giftsId;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getGiftImage() {
        return giftImage;
    }

    public void setGiftImage(String giftImage) {
        this.giftImage = giftImage;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public String getGiftDesc() {
        return giftDesc;
    }

    public void setGiftDesc(String giftDesc) {
        this.giftDesc = giftDesc;
    }
}
