package com.leonates.model;
/**
 * Created by Pelicans on 6/20/2017.
 */

public class ProductModel {

    String categoryproductId;
    String categoryProductName;
    String image1;
    String catId;
    String catName;
    String catImage;

    public ProductModel(String categoryproductId, String categoryProductName, String image1, String catId, String catName, String catImage) {
        this.categoryproductId = categoryproductId;
        this.categoryProductName = categoryProductName;
        this.image1 = image1;
        this.catId = catId;
        this.catName = catName;
        this.catImage = catImage;
    }


    public String getCategoryproductId() {
        return categoryproductId;
    }

    public void setCategoryproductId(String categoryproductId) {
        this.categoryproductId = categoryproductId;
    }

    public String getCategoryProductName() {
        return categoryProductName;
    }

    public void setCategoryProductName(String categoryProductName) {
        this.categoryProductName = categoryProductName;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getCatImage() {
        return catImage;
    }

    public void setCatImage(String catImage) {
        this.catImage = catImage;
    }
}
